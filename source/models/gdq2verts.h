
#ifndef GDQ_RENDER_GDQ2VERTS_H
#define GDQ_RENDER_GDQ2VERTS_H

#include <gccore.h>

extern f32 MODEL_GDQ2_VERTS[] ATTRIBUTE_ALIGN(32);

#endif
