
#ifndef GDQ_RENDER_GDQ2TEXCOORDS_H
#define GDQ_RENDER_GDQ2TEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_GDQ2_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
