
#ifndef GDQ_RENDER_L_NNORMS_H
#define GDQ_RENDER_L_NNORMS_H

#include <gccore.h>

extern f32 MODEL_L_N_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
