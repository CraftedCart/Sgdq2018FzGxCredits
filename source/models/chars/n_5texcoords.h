
#ifndef GDQ_RENDER_N_5TEXCOORDS_H
#define GDQ_RENDER_N_5TEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_N_5_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
