
#ifndef GDQ_RENDER_N_2VERTS_H
#define GDQ_RENDER_N_2VERTS_H

#include <gccore.h>

extern f32 MODEL_N_2_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_N_2_TRIS 56

#endif
