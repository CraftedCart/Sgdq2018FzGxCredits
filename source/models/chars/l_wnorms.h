
#ifndef GDQ_RENDER_L_WNORMS_H
#define GDQ_RENDER_L_WNORMS_H

#include <gccore.h>

extern f32 MODEL_L_W_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
