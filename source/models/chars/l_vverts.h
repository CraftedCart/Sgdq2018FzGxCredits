
#ifndef GDQ_RENDER_L_VVERTS_H
#define GDQ_RENDER_L_VVERTS_H

#include <gccore.h>

extern f32 MODEL_L_V_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_L_V_TRIS 36

#endif
