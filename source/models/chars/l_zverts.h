
#ifndef GDQ_RENDER_L_ZVERTS_H
#define GDQ_RENDER_L_ZVERTS_H

#include <gccore.h>

extern f32 MODEL_L_Z_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_L_Z_TRIS 36

#endif
