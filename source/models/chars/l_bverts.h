
#ifndef GDQ_RENDER_L_BVERTS_H
#define GDQ_RENDER_L_BVERTS_H

#include <gccore.h>

extern f32 MODEL_L_B_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_L_B_TRIS 48

#endif
