
#ifndef GDQ_RENDER_U_FTEXCOORDS_H
#define GDQ_RENDER_U_FTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_U_F_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
