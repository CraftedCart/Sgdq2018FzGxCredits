
#ifndef GDQ_RENDER_U_HVERTS_H
#define GDQ_RENDER_U_HVERTS_H

#include <gccore.h>

extern f32 MODEL_U_H_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_U_H_TRIS 44

#endif
