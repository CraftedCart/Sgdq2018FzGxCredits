
#ifndef GDQ_RENDER_L_XVERTS_H
#define GDQ_RENDER_L_XVERTS_H

#include <gccore.h>

extern f32 MODEL_L_X_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_L_X_TRIS 76

#endif
