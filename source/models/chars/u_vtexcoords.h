
#ifndef GDQ_RENDER_U_VTEXCOORDS_H
#define GDQ_RENDER_U_VTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_U_V_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
