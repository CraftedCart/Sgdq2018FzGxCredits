
#ifndef GDQ_RENDER_L_SNORMS_H
#define GDQ_RENDER_L_SNORMS_H

#include <gccore.h>

extern f32 MODEL_L_S_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
