
#ifndef GDQ_RENDER_U_CVERTS_H
#define GDQ_RENDER_U_CVERTS_H

#include <gccore.h>

extern f32 MODEL_U_C_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_U_C_TRIS 60

#endif
