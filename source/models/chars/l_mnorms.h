
#ifndef GDQ_RENDER_L_MNORMS_H
#define GDQ_RENDER_L_MNORMS_H

#include <gccore.h>

extern f32 MODEL_L_M_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
