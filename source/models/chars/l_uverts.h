
#ifndef GDQ_RENDER_L_UVERTS_H
#define GDQ_RENDER_L_UVERTS_H

#include <gccore.h>

extern f32 MODEL_L_U_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_L_U_TRIS 32

#endif
