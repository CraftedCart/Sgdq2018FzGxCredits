
#ifndef GDQ_RENDER_L_ENORMS_H
#define GDQ_RENDER_L_ENORMS_H

#include <gccore.h>

extern f32 MODEL_L_E_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
