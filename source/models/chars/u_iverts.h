
#ifndef GDQ_RENDER_U_IVERTS_H
#define GDQ_RENDER_U_IVERTS_H

#include <gccore.h>

extern f32 MODEL_U_I_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_U_I_TRIS 44

#endif
