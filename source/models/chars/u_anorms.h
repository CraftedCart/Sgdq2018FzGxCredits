
#ifndef GDQ_RENDER_U_ANORMS_H
#define GDQ_RENDER_U_ANORMS_H

#include <gccore.h>

extern f32 MODEL_U_A_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
