
#ifndef GDQ_RENDER_U_LTEXCOORDS_H
#define GDQ_RENDER_U_LTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_U_L_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
