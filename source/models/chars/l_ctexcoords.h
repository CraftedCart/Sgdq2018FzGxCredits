
#ifndef GDQ_RENDER_L_CTEXCOORDS_H
#define GDQ_RENDER_L_CTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_L_C_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
