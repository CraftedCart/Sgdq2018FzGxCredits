
#ifndef GDQ_RENDER_N_9NORMS_H
#define GDQ_RENDER_N_9NORMS_H

#include <gccore.h>

extern f32 MODEL_N_9_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
