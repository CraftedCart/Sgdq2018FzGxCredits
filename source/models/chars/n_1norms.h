
#ifndef GDQ_RENDER_N_1NORMS_H
#define GDQ_RENDER_N_1NORMS_H

#include <gccore.h>

extern f32 MODEL_N_1_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
