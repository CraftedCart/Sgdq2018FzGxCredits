
#ifndef GDQ_RENDER_U_TVERTS_H
#define GDQ_RENDER_U_TVERTS_H

#include <gccore.h>

extern f32 MODEL_U_T_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_U_T_TRIS 28

#endif
