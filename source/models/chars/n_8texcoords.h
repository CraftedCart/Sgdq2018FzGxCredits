
#ifndef GDQ_RENDER_N_8TEXCOORDS_H
#define GDQ_RENDER_N_8TEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_N_8_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
