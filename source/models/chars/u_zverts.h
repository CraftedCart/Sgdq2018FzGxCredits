
#ifndef GDQ_RENDER_U_ZVERTS_H
#define GDQ_RENDER_U_ZVERTS_H

#include <gccore.h>

extern f32 MODEL_U_Z_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_U_Z_TRIS 44

#endif
