
#ifndef GDQ_RENDER_L_RTEXCOORDS_H
#define GDQ_RENDER_L_RTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_L_R_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
