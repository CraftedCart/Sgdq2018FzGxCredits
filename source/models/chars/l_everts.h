
#ifndef GDQ_RENDER_L_EVERTS_H
#define GDQ_RENDER_L_EVERTS_H

#include <gccore.h>

extern f32 MODEL_L_E_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_L_E_TRIS 64

#endif
