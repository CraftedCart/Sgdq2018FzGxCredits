
#ifndef GDQ_RENDER_L_FNORMS_H
#define GDQ_RENDER_L_FNORMS_H

#include <gccore.h>

extern f32 MODEL_L_F_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
