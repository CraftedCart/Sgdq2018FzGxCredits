
#ifndef GDQ_RENDER_N_8VERTS_H
#define GDQ_RENDER_N_8VERTS_H

#include <gccore.h>

extern f32 MODEL_N_8_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_N_8_TRIS 76

#endif
