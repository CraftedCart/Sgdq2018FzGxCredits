
#ifndef GDQ_RENDER_U_FVERTS_H
#define GDQ_RENDER_U_FVERTS_H

#include <gccore.h>

extern f32 MODEL_U_F_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_U_F_TRIS 36

#endif
