
#ifndef GDQ_RENDER_L_ONORMS_H
#define GDQ_RENDER_L_ONORMS_H

#include <gccore.h>

extern f32 MODEL_L_O_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
