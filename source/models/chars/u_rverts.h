
#ifndef GDQ_RENDER_U_RVERTS_H
#define GDQ_RENDER_U_RVERTS_H

#include <gccore.h>

extern f32 MODEL_U_R_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_U_R_TRIS 60

#endif
