
#ifndef GDQ_RENDER_N_9VERTS_H
#define GDQ_RENDER_N_9VERTS_H

#include <gccore.h>

extern f32 MODEL_N_9_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_N_9_TRIS 68

#endif
