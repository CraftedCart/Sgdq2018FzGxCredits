
#ifndef GDQ_RENDER_U_LNORMS_H
#define GDQ_RENDER_U_LNORMS_H

#include <gccore.h>

extern f32 MODEL_U_L_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
