
#ifndef GDQ_RENDER_U_UTEXCOORDS_H
#define GDQ_RENDER_U_UTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_U_U_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
