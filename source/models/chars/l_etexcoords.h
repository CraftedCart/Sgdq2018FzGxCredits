
#ifndef GDQ_RENDER_L_ETEXCOORDS_H
#define GDQ_RENDER_L_ETEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_L_E_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
