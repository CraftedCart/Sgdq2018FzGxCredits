
#ifndef GDQ_RENDER_L_FVERTS_H
#define GDQ_RENDER_L_FVERTS_H

#include <gccore.h>

extern f32 MODEL_L_F_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_L_F_TRIS 56

#endif
