
#ifndef GDQ_RENDER_L_FTEXCOORDS_H
#define GDQ_RENDER_L_FTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_L_F_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
