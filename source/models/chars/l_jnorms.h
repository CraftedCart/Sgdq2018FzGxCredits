
#ifndef GDQ_RENDER_L_JNORMS_H
#define GDQ_RENDER_L_JNORMS_H

#include <gccore.h>

extern f32 MODEL_L_J_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
