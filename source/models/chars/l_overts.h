
#ifndef GDQ_RENDER_L_OVERTS_H
#define GDQ_RENDER_L_OVERTS_H

#include <gccore.h>

extern f32 MODEL_L_O_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_L_O_TRIS 52

#endif
