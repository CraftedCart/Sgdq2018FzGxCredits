
#ifndef GDQ_RENDER_S_DASHNORMS_H
#define GDQ_RENDER_S_DASHNORMS_H

#include <gccore.h>

extern f32 MODEL_S_DASH_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
