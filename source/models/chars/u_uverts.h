
#ifndef GDQ_RENDER_U_UVERTS_H
#define GDQ_RENDER_U_UVERTS_H

#include <gccore.h>

extern f32 MODEL_U_U_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_U_U_TRIS 36

#endif
