
#ifndef GDQ_RENDER_L_QTEXCOORDS_H
#define GDQ_RENDER_L_QTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_L_Q_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
