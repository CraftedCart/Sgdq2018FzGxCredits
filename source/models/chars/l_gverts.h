
#ifndef GDQ_RENDER_L_GVERTS_H
#define GDQ_RENDER_L_GVERTS_H

#include <gccore.h>

extern f32 MODEL_L_G_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_L_G_TRIS 64

#endif
