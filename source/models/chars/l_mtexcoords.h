
#ifndef GDQ_RENDER_L_MTEXCOORDS_H
#define GDQ_RENDER_L_MTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_L_M_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
