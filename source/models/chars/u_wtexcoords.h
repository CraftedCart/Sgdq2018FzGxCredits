
#ifndef GDQ_RENDER_U_WTEXCOORDS_H
#define GDQ_RENDER_U_WTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_U_W_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
