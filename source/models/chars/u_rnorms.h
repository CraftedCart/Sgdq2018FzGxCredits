
#ifndef GDQ_RENDER_U_RNORMS_H
#define GDQ_RENDER_U_RNORMS_H

#include <gccore.h>

extern f32 MODEL_U_R_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
