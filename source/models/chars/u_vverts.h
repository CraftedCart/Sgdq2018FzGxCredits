
#ifndef GDQ_RENDER_U_VVERTS_H
#define GDQ_RENDER_U_VVERTS_H

#include <gccore.h>

extern f32 MODEL_U_V_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_U_V_TRIS 40

#endif
