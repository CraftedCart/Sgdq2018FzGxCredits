
#ifndef GDQ_RENDER_L_DTEXCOORDS_H
#define GDQ_RENDER_L_DTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_L_D_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
