
#ifndef GDQ_RENDER_U_YVERTS_H
#define GDQ_RENDER_U_YVERTS_H

#include <gccore.h>

extern f32 MODEL_U_Y_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_U_Y_TRIS 40

#endif
