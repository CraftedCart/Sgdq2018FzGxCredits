
#ifndef GDQ_RENDER_S_DASHVERTS_H
#define GDQ_RENDER_S_DASHVERTS_H

#include <gccore.h>

extern f32 MODEL_S_DASH_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_S_DASH_TRIS 12

#endif
