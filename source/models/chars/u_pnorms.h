
#ifndef GDQ_RENDER_U_PNORMS_H
#define GDQ_RENDER_U_PNORMS_H

#include <gccore.h>

extern f32 MODEL_U_P_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
