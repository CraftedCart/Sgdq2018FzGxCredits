
#ifndef GDQ_RENDER_U_TNORMS_H
#define GDQ_RENDER_U_TNORMS_H

#include <gccore.h>

extern f32 MODEL_U_T_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
