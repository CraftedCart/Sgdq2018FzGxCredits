
#ifndef GDQ_RENDER_L_WTEXCOORDS_H
#define GDQ_RENDER_L_WTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_L_W_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
