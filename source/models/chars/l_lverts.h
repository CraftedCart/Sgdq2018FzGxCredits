
#ifndef GDQ_RENDER_L_LVERTS_H
#define GDQ_RENDER_L_LVERTS_H

#include <gccore.h>

extern f32 MODEL_L_L_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_L_L_TRIS 36

#endif
