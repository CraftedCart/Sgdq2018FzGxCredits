
#ifndef GDQ_RENDER_U_LVERTS_H
#define GDQ_RENDER_U_LVERTS_H

#include <gccore.h>

extern f32 MODEL_U_L_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_U_L_TRIS 20

#endif
