
#ifndef GDQ_RENDER_N_7VERTS_H
#define GDQ_RENDER_N_7VERTS_H

#include <gccore.h>

extern f32 MODEL_N_7_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_N_7_TRIS 44

#endif
