
#ifndef GDQ_RENDER_L_KNORMS_H
#define GDQ_RENDER_L_KNORMS_H

#include <gccore.h>

extern f32 MODEL_L_K_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
