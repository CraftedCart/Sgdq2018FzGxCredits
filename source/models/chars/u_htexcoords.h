
#ifndef GDQ_RENDER_U_HTEXCOORDS_H
#define GDQ_RENDER_U_HTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_U_H_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
