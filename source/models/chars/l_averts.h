
#ifndef GDQ_RENDER_L_AVERTS_H
#define GDQ_RENDER_L_AVERTS_H

#include <gccore.h>

extern f32 MODEL_L_A_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_L_A_TRIS 64

#endif
