
#ifndef GDQ_RENDER_N_4VERTS_H
#define GDQ_RENDER_N_4VERTS_H

#include <gccore.h>

extern f32 MODEL_N_4_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_N_4_TRIS 48

#endif
