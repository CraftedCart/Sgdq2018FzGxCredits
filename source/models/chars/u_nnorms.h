
#ifndef GDQ_RENDER_U_NNORMS_H
#define GDQ_RENDER_U_NNORMS_H

#include <gccore.h>

extern f32 MODEL_U_N_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
