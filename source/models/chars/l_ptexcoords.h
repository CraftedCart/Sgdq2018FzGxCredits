
#ifndef GDQ_RENDER_L_PTEXCOORDS_H
#define GDQ_RENDER_L_PTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_L_P_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
