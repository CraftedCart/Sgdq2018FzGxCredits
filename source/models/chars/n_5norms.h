
#ifndef GDQ_RENDER_N_5NORMS_H
#define GDQ_RENDER_N_5NORMS_H

#include <gccore.h>

extern f32 MODEL_N_5_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
