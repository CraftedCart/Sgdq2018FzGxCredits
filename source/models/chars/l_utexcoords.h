
#ifndef GDQ_RENDER_L_UTEXCOORDS_H
#define GDQ_RENDER_L_UTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_L_U_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
