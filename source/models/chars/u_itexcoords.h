
#ifndef GDQ_RENDER_U_ITEXCOORDS_H
#define GDQ_RENDER_U_ITEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_U_I_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
