
#ifndef GDQ_RENDER_U_AVERTS_H
#define GDQ_RENDER_U_AVERTS_H

#include <gccore.h>

extern f32 MODEL_U_A_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_U_A_TRIS 60

#endif
