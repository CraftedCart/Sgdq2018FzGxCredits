
#ifndef GDQ_RENDER_L_TVERTS_H
#define GDQ_RENDER_L_TVERTS_H

#include <gccore.h>

extern f32 MODEL_L_T_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_L_T_TRIS 56

#endif
