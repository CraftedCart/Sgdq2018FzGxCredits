
#ifndef GDQ_RENDER_N_3TEXCOORDS_H
#define GDQ_RENDER_N_3TEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_N_3_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
