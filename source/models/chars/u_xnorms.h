
#ifndef GDQ_RENDER_U_XNORMS_H
#define GDQ_RENDER_U_XNORMS_H

#include <gccore.h>

extern f32 MODEL_U_X_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
