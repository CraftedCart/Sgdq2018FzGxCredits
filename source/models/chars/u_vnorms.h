
#ifndef GDQ_RENDER_U_VNORMS_H
#define GDQ_RENDER_U_VNORMS_H

#include <gccore.h>

extern f32 MODEL_U_V_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
