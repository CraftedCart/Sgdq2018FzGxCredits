
#ifndef GDQ_RENDER_N_9TEXCOORDS_H
#define GDQ_RENDER_N_9TEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_N_9_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
