
#ifndef GDQ_RENDER_L_BNORMS_H
#define GDQ_RENDER_L_BNORMS_H

#include <gccore.h>

extern f32 MODEL_L_B_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
