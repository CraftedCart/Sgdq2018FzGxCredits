
#ifndef GDQ_RENDER_L_KVERTS_H
#define GDQ_RENDER_L_KVERTS_H

#include <gccore.h>

extern f32 MODEL_L_K_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_L_K_TRIS 40

#endif
