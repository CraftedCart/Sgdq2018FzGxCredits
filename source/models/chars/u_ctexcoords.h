
#ifndef GDQ_RENDER_U_CTEXCOORDS_H
#define GDQ_RENDER_U_CTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_U_C_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
