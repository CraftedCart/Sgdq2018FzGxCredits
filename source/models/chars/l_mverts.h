
#ifndef GDQ_RENDER_L_MVERTS_H
#define GDQ_RENDER_L_MVERTS_H

#include <gccore.h>

extern f32 MODEL_L_M_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_L_M_TRIS 48

#endif
