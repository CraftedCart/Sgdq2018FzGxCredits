
#ifndef GDQ_RENDER_L_UNORMS_H
#define GDQ_RENDER_L_UNORMS_H

#include <gccore.h>

extern f32 MODEL_L_U_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
