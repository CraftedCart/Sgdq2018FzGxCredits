
#ifndef GDQ_RENDER_L_ANORMS_H
#define GDQ_RENDER_L_ANORMS_H

#include <gccore.h>

extern f32 MODEL_L_A_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
