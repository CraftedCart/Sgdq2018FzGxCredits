
#ifndef GDQ_RENDER_L_GTEXCOORDS_H
#define GDQ_RENDER_L_GTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_L_G_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
