
#ifndef GDQ_RENDER_L_VTEXCOORDS_H
#define GDQ_RENDER_L_VTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_L_V_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
