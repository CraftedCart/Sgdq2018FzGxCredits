
#ifndef GDQ_RENDER_L_NVERTS_H
#define GDQ_RENDER_L_NVERTS_H

#include <gccore.h>

extern f32 MODEL_L_N_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_L_N_TRIS 32

#endif
