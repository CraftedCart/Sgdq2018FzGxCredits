
#ifndef GDQ_RENDER_U_DNORMS_H
#define GDQ_RENDER_U_DNORMS_H

#include <gccore.h>

extern f32 MODEL_U_D_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
