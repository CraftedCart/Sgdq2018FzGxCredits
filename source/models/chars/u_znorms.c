
#include "u_znorms.h"

f32 MODEL_U_Z_NORMS[] ATTRIBUTE_ALIGN(32) = {
    -0.0f, 1.0f, -0.0f,    -0.0f, 1.0f, -0.0f,    -0.0f, 1.0f, -0.0f,
    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,
    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,
    0.0f, 0.9999999403953552f, -0.0f,    0.0f, 0.9999999403953552f, -0.0f,    0.0f, 0.9999999403953552f, -0.0f,
    -0.0f, 1.0f, -1.0050977827802399e-08f,    -0.0f, 1.0f, -1.0050977827802399e-08f,    -0.0f, 1.0f, -1.0050977827802399e-08f,
    0.0f, 0.9999999403953552f, -1.0050977827802399e-08f,    0.0f, 0.9999999403953552f, -1.0050977827802399e-08f,    0.0f, 0.9999999403953552f, -1.0050977827802399e-08f,
    -0.0f, 0.9999999403953552f, -0.0f,    -0.0f, 0.9999999403953552f, -0.0f,    -0.0f, 0.9999999403953552f, -0.0f,
    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,
    -0.0f, 1.0f, -0.0f,    -0.0f, 1.0f, -0.0f,    -0.0f, 1.0f, -0.0f,
    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,
    0.0f, -1.0f, -0.0f,    0.0f, -1.0f, -0.0f,    0.0f, -1.0f, -0.0f,
    0.0f, -1.0f, 0.0f,    0.0f, -1.0f, 0.0f,    0.0f, -1.0f, 0.0f,
    0.0f, -1.0f, 0.0f,    0.0f, -1.0f, 0.0f,    0.0f, -1.0f, 0.0f,
    0.0f, -0.9999999403953552f, -0.0f,    0.0f, -0.9999999403953552f, -0.0f,    0.0f, -0.9999999403953552f, -0.0f,
    0.0f, -1.0f, 0.0f,    0.0f, -1.0f, 0.0f,    0.0f, -1.0f, 0.0f,
    0.0f, -0.9999999403953552f, 0.0f,    0.0f, -0.9999999403953552f, 0.0f,    0.0f, -0.9999999403953552f, 0.0f,
    0.0f, -0.9999999403953552f, -0.0f,    0.0f, -0.9999999403953552f, -0.0f,    0.0f, -0.9999999403953552f, -0.0f,
    0.0f, -1.0f, 0.0f,    0.0f, -1.0f, 0.0f,    0.0f, -1.0f, 0.0f,
    0.0f, -1.0f, 0.0f,    0.0f, -1.0f, 0.0f,    0.0f, -1.0f, 0.0f,
    0.0f, -1.0f, 0.0f,    0.0f, -1.0f, 0.0f,    0.0f, -1.0f, 0.0f,
    -0.0f, -0.0f, 0.9999999403953552f,    -0.0f, -0.0f, 0.9999999403953552f,    -0.0f, -0.0f, 0.9999999403953552f,
    -1.0f, 0.0f, -0.0f,    -1.0f, 0.0f, -0.0f,    -1.0f, 0.0f, -0.0f,
    -0.0f, 0.0f, -0.9999999403953552f,    -0.0f, 0.0f, -0.9999999403953552f,    -0.0f, 0.0f, -0.9999999403953552f,
    1.0f, 0.0f, -0.0f,    1.0f, 0.0f, -0.0f,    1.0f, 0.0f, -0.0f,
    0.699065089225769f, 0.0f, 0.7150580286979675f,    0.699065089225769f, 0.0f, 0.7150580286979675f,    0.699065089225769f, 0.0f, 0.7150580286979675f,
    1.0f, 0.0f, -0.0f,    1.0f, 0.0f, -0.0f,    1.0f, 0.0f, -0.0f,
    -0.0f, 0.0f, -1.0f,    -0.0f, 0.0f, -1.0f,    -0.0f, 0.0f, -1.0f,
    1.0f, 0.0f, -0.0f,    1.0f, 0.0f, -0.0f,    1.0f, 0.0f, -0.0f,
    -0.0f, -0.0f, 0.9999999403953552f,    -0.0f, -0.0f, 0.9999999403953552f,    -0.0f, -0.0f, 0.9999999403953552f,
    -1.0f, 0.0f, -0.0f,    -1.0f, 0.0f, -0.0f,    -1.0f, 0.0f, -0.0f,
    -0.6990659236907959f, 0.0f, -0.7150571346282959f,    -0.6990659236907959f, 0.0f, -0.7150571346282959f,    -0.6990659236907959f, 0.0f, -0.7150571346282959f,
    -1.0f, 0.0f, -0.0f,    -1.0f, 0.0f, -0.0f,    -1.0f, 0.0f, -0.0f,
    0.0f, 0.0f, 0.9999999403953552f,    0.0f, 0.0f, 0.9999999403953552f,    0.0f, 0.0f, 0.9999999403953552f,
    -1.0f, 0.0f, -0.0f,    -1.0f, 0.0f, -0.0f,    -1.0f, 0.0f, -0.0f,
    0.0f, 0.0f, -0.9999999403953552f,    0.0f, 0.0f, -0.9999999403953552f,    0.0f, 0.0f, -0.9999999403953552f,
    1.0f, -0.0f, -0.0f,    1.0f, -0.0f, -0.0f,    1.0f, -0.0f, -0.0f,
    0.699065089225769f, 0.0f, 0.7150580286979675f,    0.699065089225769f, 0.0f, 0.7150580286979675f,    0.699065089225769f, 0.0f, 0.7150580286979675f,
    1.0f, -0.0f, -0.0f,    1.0f, -0.0f, -0.0f,    1.0f, -0.0f, -0.0f,
    0.0f, 0.0f, -1.0f,    0.0f, 0.0f, -1.0f,    0.0f, 0.0f, -1.0f,
    1.0f, -0.0f, -0.0f,    1.0f, -0.0f, -0.0f,    1.0f, -0.0f, -0.0f,
    0.0f, 0.0f, 0.9999999403953552f,    0.0f, 0.0f, 0.9999999403953552f,    0.0f, 0.0f, 0.9999999403953552f,
    -1.0f, 0.0f, -0.0f,    -1.0f, 0.0f, -0.0f,    -1.0f, 0.0f, -0.0f,
    -0.6990660429000854f, 0.0f, -0.7150571942329407f,    -0.6990660429000854f, 0.0f, -0.7150571942329407f,    -0.6990660429000854f, 0.0f, -0.7150571942329407f,
    -1.0f, 0.0f, -0.0f,    -1.0f, 0.0f, -0.0f,    -1.0f, 0.0f, -0.0f,

};
