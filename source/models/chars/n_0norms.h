
#ifndef GDQ_RENDER_N_0NORMS_H
#define GDQ_RENDER_N_0NORMS_H

#include <gccore.h>

extern f32 MODEL_N_0_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
