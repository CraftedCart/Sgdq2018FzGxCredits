
#ifndef GDQ_RENDER_U_BVERTS_H
#define GDQ_RENDER_U_BVERTS_H

#include <gccore.h>

extern f32 MODEL_U_B_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_U_B_TRIS 72

#endif
