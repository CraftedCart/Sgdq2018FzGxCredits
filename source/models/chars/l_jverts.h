
#ifndef GDQ_RENDER_L_JVERTS_H
#define GDQ_RENDER_L_JVERTS_H

#include <gccore.h>

extern f32 MODEL_L_J_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_L_J_TRIS 56

#endif
