
#ifndef GDQ_RENDER_U_QNORMS_H
#define GDQ_RENDER_U_QNORMS_H

#include <gccore.h>

extern f32 MODEL_U_Q_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
