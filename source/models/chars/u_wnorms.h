
#ifndef GDQ_RENDER_U_WNORMS_H
#define GDQ_RENDER_U_WNORMS_H

#include <gccore.h>

extern f32 MODEL_U_W_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
