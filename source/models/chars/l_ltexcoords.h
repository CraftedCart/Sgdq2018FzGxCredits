
#ifndef GDQ_RENDER_L_LTEXCOORDS_H
#define GDQ_RENDER_L_LTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_L_L_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
