
#ifndef GDQ_RENDER_U_GTEXCOORDS_H
#define GDQ_RENDER_U_GTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_U_G_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
