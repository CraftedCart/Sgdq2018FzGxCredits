
#ifndef GDQ_RENDER_L_HNORMS_H
#define GDQ_RENDER_L_HNORMS_H

#include <gccore.h>

extern f32 MODEL_L_H_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
