
#ifndef GDQ_RENDER_L_OTEXCOORDS_H
#define GDQ_RENDER_L_OTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_L_O_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
