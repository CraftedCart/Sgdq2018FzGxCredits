
#ifndef GDQ_RENDER_U_MTEXCOORDS_H
#define GDQ_RENDER_U_MTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_U_M_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
