
#ifndef GDQ_RENDER_U_SVERTS_H
#define GDQ_RENDER_U_SVERTS_H

#include <gccore.h>

extern f32 MODEL_U_S_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_U_S_TRIS 84

#endif
