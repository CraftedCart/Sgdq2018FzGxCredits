
#ifndef GDQ_RENDER_U_BNORMS_H
#define GDQ_RENDER_U_BNORMS_H

#include <gccore.h>

extern f32 MODEL_U_B_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
