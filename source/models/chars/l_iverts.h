
#ifndef GDQ_RENDER_L_IVERTS_H
#define GDQ_RENDER_L_IVERTS_H

#include <gccore.h>

extern f32 MODEL_L_I_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_L_I_TRIS 48

#endif
