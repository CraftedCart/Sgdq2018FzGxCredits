
#ifndef GDQ_RENDER_N_7TEXCOORDS_H
#define GDQ_RENDER_N_7TEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_N_7_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
