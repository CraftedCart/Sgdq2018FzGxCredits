
#ifndef GDQ_RENDER_L_NTEXCOORDS_H
#define GDQ_RENDER_L_NTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_L_N_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
