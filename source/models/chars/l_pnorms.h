
#ifndef GDQ_RENDER_L_PNORMS_H
#define GDQ_RENDER_L_PNORMS_H

#include <gccore.h>

extern f32 MODEL_L_P_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
