
#ifndef GDQ_RENDER_U_KTEXCOORDS_H
#define GDQ_RENDER_U_KTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_U_K_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
