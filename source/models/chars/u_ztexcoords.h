
#ifndef GDQ_RENDER_U_ZTEXCOORDS_H
#define GDQ_RENDER_U_ZTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_U_Z_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
