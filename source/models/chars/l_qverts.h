
#ifndef GDQ_RENDER_L_QVERTS_H
#define GDQ_RENDER_L_QVERTS_H

#include <gccore.h>

extern f32 MODEL_L_Q_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_L_Q_TRIS 52

#endif
