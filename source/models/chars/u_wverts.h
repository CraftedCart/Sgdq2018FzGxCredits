
#ifndef GDQ_RENDER_U_WVERTS_H
#define GDQ_RENDER_U_WVERTS_H

#include <gccore.h>

extern f32 MODEL_U_W_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_U_W_TRIS 48

#endif
