
#ifndef GDQ_RENDER_U_MVERTS_H
#define GDQ_RENDER_U_MVERTS_H

#include <gccore.h>

extern f32 MODEL_U_M_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_U_M_TRIS 48

#endif
