
#ifndef GDQ_RENDER_N_0TEXCOORDS_H
#define GDQ_RENDER_N_0TEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_N_0_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
