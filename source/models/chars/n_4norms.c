
#include "n_4norms.h"

f32 MODEL_N_4_NORMS[] ATTRIBUTE_ALIGN(32) = {
    -0.0f, 1.0f, -0.0f,    -0.0f, 1.0f, -0.0f,    -0.0f, 1.0f, -0.0f,
    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,
    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,
    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,
    0.0f, 0.9999999403953552f, -0.0f,    0.0f, 0.9999999403953552f, -0.0f,    0.0f, 0.9999999403953552f, -0.0f,
    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,
    -0.0f, 1.0f, -0.0f,    -0.0f, 1.0f, -0.0f,    -0.0f, 1.0f, -0.0f,
    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,
    -0.0f, 1.0f, -0.0f,    -0.0f, 1.0f, -0.0f,    -0.0f, 1.0f, -0.0f,
    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,
    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,
    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,
    0.0f, -1.0f, 0.0f,    0.0f, -1.0f, 0.0f,    0.0f, -1.0f, 0.0f,
    0.0f, -1.0f, 0.0f,    0.0f, -1.0f, 0.0f,    0.0f, -1.0f, 0.0f,
    0.0f, -1.0f, 0.0f,    0.0f, -1.0f, 0.0f,    0.0f, -1.0f, 0.0f,
    3.292822237099813e-09f, -1.0f, 2.4565494083361727e-09f,    3.292822237099813e-09f, -1.0f, 2.4565494083361727e-09f,    3.292822237099813e-09f, -1.0f, 2.4565494083361727e-09f,
    0.0f, -0.9999999403953552f, 0.0f,    0.0f, -0.9999999403953552f, 0.0f,    0.0f, -0.9999999403953552f, 0.0f,
    1.6359576804347853e-08f, -1.0f, -0.0f,    1.6359576804347853e-08f, -1.0f, -0.0f,    1.6359576804347853e-08f, -1.0f, -0.0f,
    0.0f, -1.0f, -0.0f,    0.0f, -1.0f, -0.0f,    0.0f, -1.0f, -0.0f,
    0.0f, -1.0f, 0.0f,    0.0f, -1.0f, 0.0f,    0.0f, -1.0f, 0.0f,
    0.0f, -1.0f, 0.0f,    0.0f, -1.0f, 0.0f,    0.0f, -1.0f, 0.0f,
    0.0f, -1.0f, 0.0f,    0.0f, -1.0f, 0.0f,    0.0f, -1.0f, 0.0f,
    1.6359576804347853e-08f, -1.0f, -0.0f,    1.6359576804347853e-08f, -1.0f, -0.0f,    1.6359576804347853e-08f, -1.0f, -0.0f,
    0.0f, -1.0f, 9.794904443083396e-09f,    0.0f, -1.0f, 9.794904443083396e-09f,    0.0f, -1.0f, 9.794904443083396e-09f,
    -1.0f, 0.0f, -0.0f,    -1.0f, 0.0f, -0.0f,    -1.0f, 0.0f, -0.0f,
    -0.7146000862121582f, 0.0f, -0.6995332837104797f,    -0.7146000862121582f, 0.0f, -0.6995332837104797f,    -0.7146000862121582f, 0.0f, -0.6995332837104797f,
    -0.0f, 0.0f, -1.0f,    -0.0f, 0.0f, -1.0f,    -0.0f, 0.0f, -1.0f,
    1.0f, 0.0f, -0.0f,    1.0f, 0.0f, -0.0f,    1.0f, 0.0f, -0.0f,
    -0.0f, -0.0f, 1.0f,    -0.0f, -0.0f, 1.0f,    -0.0f, -0.0f, 1.0f,
    -1.0f, 0.0f, -0.0f,    -1.0f, 0.0f, -0.0f,    -1.0f, 0.0f, -0.0f,
    -0.0f, -0.0f, 1.0f,    -0.0f, -0.0f, 1.0f,    -0.0f, -0.0f, 1.0f,
    -1.0f, 0.0f, -0.0f,    -1.0f, 0.0f, -0.0f,    -1.0f, 0.0f, -0.0f,
    -0.0f, -0.0f, 1.0f,    -0.0f, -0.0f, 1.0f,    -0.0f, -0.0f, 1.0f,
    0.7174781560897827f, 0.0f, 0.6965810060501099f,    0.7174781560897827f, 0.0f, 0.6965810060501099f,    0.7174781560897827f, 0.0f, 0.6965810060501099f,
    1.0f, 0.0f, -0.0f,    1.0f, 0.0f, -0.0f,    1.0f, 0.0f, -0.0f,
    -0.0f, 0.0f, -1.0f,    -0.0f, 0.0f, -1.0f,    -0.0f, 0.0f, -1.0f,
    -1.0f, 0.0f, -0.0f,    -1.0f, 0.0f, -0.0f,    -1.0f, 0.0f, -0.0f,
    -0.7146000862121582f, 0.0f, -0.6995332837104797f,    -0.7146000862121582f, 0.0f, -0.6995332837104797f,    -0.7146000862121582f, 0.0f, -0.6995332837104797f,
    0.0f, 0.0f, -1.0f,    0.0f, 0.0f, -1.0f,    0.0f, 0.0f, -1.0f,
    1.0f, -0.0f, -0.0f,    1.0f, -0.0f, -0.0f,    1.0f, -0.0f, -0.0f,
    0.0f, 0.0f, 1.0f,    0.0f, 0.0f, 1.0f,    0.0f, 0.0f, 1.0f,
    -1.0f, 0.0f, -0.0f,    -1.0f, 0.0f, -0.0f,    -1.0f, 0.0f, -0.0f,
    0.0f, 0.0f, 1.0f,    0.0f, 0.0f, 1.0f,    0.0f, 0.0f, 1.0f,
    -1.0f, 0.0f, -0.0f,    -1.0f, 0.0f, -0.0f,    -1.0f, 0.0f, -0.0f,
    0.0f, 0.0f, 1.0f,    0.0f, 0.0f, 1.0f,    0.0f, 0.0f, 1.0f,
    0.7174781560897827f, 0.0f, 0.6965810060501099f,    0.7174781560897827f, 0.0f, 0.6965810060501099f,    0.7174781560897827f, 0.0f, 0.6965810060501099f,
    1.0f, -0.0f, -0.0f,    1.0f, -0.0f, -0.0f,    1.0f, -0.0f, -0.0f,
    0.0f, 0.0f, -1.0f,    0.0f, 0.0f, -1.0f,    0.0f, 0.0f, -1.0f,

};
