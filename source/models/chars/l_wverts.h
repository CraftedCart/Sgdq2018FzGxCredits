
#ifndef GDQ_RENDER_L_WVERTS_H
#define GDQ_RENDER_L_WVERTS_H

#include <gccore.h>

extern f32 MODEL_L_W_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_L_W_TRIS 52

#endif
