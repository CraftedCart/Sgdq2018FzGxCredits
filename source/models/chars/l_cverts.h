
#ifndef GDQ_RENDER_L_CVERTS_H
#define GDQ_RENDER_L_CVERTS_H

#include <gccore.h>

extern f32 MODEL_L_C_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_L_C_TRIS 52

#endif
