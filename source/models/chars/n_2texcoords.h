
#ifndef GDQ_RENDER_N_2TEXCOORDS_H
#define GDQ_RENDER_N_2TEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_N_2_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
