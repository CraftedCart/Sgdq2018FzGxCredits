
#ifndef GDQ_RENDER_L_STEXCOORDS_H
#define GDQ_RENDER_L_STEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_L_S_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
