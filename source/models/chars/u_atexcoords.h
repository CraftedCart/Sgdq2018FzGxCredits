
#ifndef GDQ_RENDER_U_ATEXCOORDS_H
#define GDQ_RENDER_U_ATEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_U_A_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
