
#ifndef GDQ_RENDER_L_ZNORMS_H
#define GDQ_RENDER_L_ZNORMS_H

#include <gccore.h>

extern f32 MODEL_L_Z_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
