
#ifndef GDQ_RENDER_L_ITEXCOORDS_H
#define GDQ_RENDER_L_ITEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_L_I_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
