
#ifndef GDQ_RENDER_L_YTEXCOORDS_H
#define GDQ_RENDER_L_YTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_L_Y_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
