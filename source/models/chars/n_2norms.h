
#ifndef GDQ_RENDER_N_2NORMS_H
#define GDQ_RENDER_N_2NORMS_H

#include <gccore.h>

extern f32 MODEL_N_2_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
