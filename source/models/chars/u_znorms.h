
#ifndef GDQ_RENDER_U_ZNORMS_H
#define GDQ_RENDER_U_ZNORMS_H

#include <gccore.h>

extern f32 MODEL_U_Z_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
