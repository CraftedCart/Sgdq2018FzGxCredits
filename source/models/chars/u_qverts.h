
#ifndef GDQ_RENDER_U_QVERTS_H
#define GDQ_RENDER_U_QVERTS_H

#include <gccore.h>

extern f32 MODEL_U_Q_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_U_Q_TRIS 72

#endif
