
#ifndef GDQ_RENDER_L_SVERTS_H
#define GDQ_RENDER_L_SVERTS_H

#include <gccore.h>

extern f32 MODEL_L_S_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_L_S_TRIS 56

#endif
