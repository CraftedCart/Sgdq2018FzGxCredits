
#ifndef GDQ_RENDER_L_HVERTS_H
#define GDQ_RENDER_L_HVERTS_H

#include <gccore.h>

extern f32 MODEL_L_H_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_L_H_TRIS 40

#endif
