
#ifndef GDQ_RENDER_U_XVERTS_H
#define GDQ_RENDER_U_XVERTS_H

#include <gccore.h>

extern f32 MODEL_U_X_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_U_X_TRIS 60

#endif
