
#ifndef GDQ_RENDER_N_1VERTS_H
#define GDQ_RENDER_N_1VERTS_H

#include <gccore.h>

extern f32 MODEL_N_1_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_N_1_TRIS 40

#endif
