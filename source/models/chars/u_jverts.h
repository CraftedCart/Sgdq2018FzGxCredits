
#ifndef GDQ_RENDER_U_JVERTS_H
#define GDQ_RENDER_U_JVERTS_H

#include <gccore.h>

extern f32 MODEL_U_J_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_U_J_TRIS 52

#endif
