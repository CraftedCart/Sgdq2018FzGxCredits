
#ifndef GDQ_RENDER_L_YNORMS_H
#define GDQ_RENDER_L_YNORMS_H

#include <gccore.h>

extern f32 MODEL_L_Y_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
