
#ifndef GDQ_RENDER_L_LNORMS_H
#define GDQ_RENDER_L_LNORMS_H

#include <gccore.h>

extern f32 MODEL_L_L_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
