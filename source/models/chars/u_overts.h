
#ifndef GDQ_RENDER_U_OVERTS_H
#define GDQ_RENDER_U_OVERTS_H

#include <gccore.h>

extern f32 MODEL_U_O_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_U_O_TRIS 52

#endif
