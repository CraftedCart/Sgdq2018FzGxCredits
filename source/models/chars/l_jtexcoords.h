
#ifndef GDQ_RENDER_L_JTEXCOORDS_H
#define GDQ_RENDER_L_JTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_L_J_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
