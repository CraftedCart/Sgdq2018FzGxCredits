
#ifndef GDQ_RENDER_N_4TEXCOORDS_H
#define GDQ_RENDER_N_4TEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_N_4_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
