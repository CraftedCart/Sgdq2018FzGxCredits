
#ifndef GDQ_RENDER_N_0VERTS_H
#define GDQ_RENDER_N_0VERTS_H

#include <gccore.h>

extern f32 MODEL_N_0_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_N_0_TRIS 76

#endif
