
#ifndef GDQ_RENDER_U_GVERTS_H
#define GDQ_RENDER_U_GVERTS_H

#include <gccore.h>

extern f32 MODEL_U_G_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_U_G_TRIS 68

#endif
