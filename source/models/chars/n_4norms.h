
#ifndef GDQ_RENDER_N_4NORMS_H
#define GDQ_RENDER_N_4NORMS_H

#include <gccore.h>

extern f32 MODEL_N_4_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
