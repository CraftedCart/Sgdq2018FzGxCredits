
#ifndef GDQ_RENDER_U_QTEXCOORDS_H
#define GDQ_RENDER_U_QTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_U_Q_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
