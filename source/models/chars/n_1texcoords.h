
#ifndef GDQ_RENDER_N_1TEXCOORDS_H
#define GDQ_RENDER_N_1TEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_N_1_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
