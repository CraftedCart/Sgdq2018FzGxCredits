
#ifndef GDQ_RENDER_N_6VERTS_H
#define GDQ_RENDER_N_6VERTS_H

#include <gccore.h>

extern f32 MODEL_N_6_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_N_6_TRIS 64

#endif
