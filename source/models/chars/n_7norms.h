
#ifndef GDQ_RENDER_N_7NORMS_H
#define GDQ_RENDER_N_7NORMS_H

#include <gccore.h>

extern f32 MODEL_N_7_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
