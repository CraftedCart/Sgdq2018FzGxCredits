
#ifndef GDQ_RENDER_L_RNORMS_H
#define GDQ_RENDER_L_RNORMS_H

#include <gccore.h>

extern f32 MODEL_L_R_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
