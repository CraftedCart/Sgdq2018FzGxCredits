
#ifndef GDQ_RENDER_L_XTEXCOORDS_H
#define GDQ_RENDER_L_XTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_L_X_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
