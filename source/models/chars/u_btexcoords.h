
#ifndef GDQ_RENDER_U_BTEXCOORDS_H
#define GDQ_RENDER_U_BTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_U_B_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
