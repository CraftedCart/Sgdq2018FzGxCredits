
#ifndef GDQ_RENDER_L_YVERTS_H
#define GDQ_RENDER_L_YVERTS_H

#include <gccore.h>

extern f32 MODEL_L_Y_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_L_Y_TRIS 52

#endif
