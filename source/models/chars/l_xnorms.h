
#ifndef GDQ_RENDER_L_XNORMS_H
#define GDQ_RENDER_L_XNORMS_H

#include <gccore.h>

extern f32 MODEL_L_X_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
