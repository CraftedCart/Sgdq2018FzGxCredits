
#ifndef GDQ_RENDER_N_5VERTS_H
#define GDQ_RENDER_N_5VERTS_H

#include <gccore.h>

extern f32 MODEL_N_5_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_N_5_TRIS 60

#endif
