
#ifndef GDQ_RENDER_U_NVERTS_H
#define GDQ_RENDER_U_NVERTS_H

#include <gccore.h>

extern f32 MODEL_U_N_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_U_N_TRIS 44

#endif
