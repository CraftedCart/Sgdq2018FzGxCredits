
#ifndef GDQ_RENDER_U_STEXCOORDS_H
#define GDQ_RENDER_U_STEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_U_S_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
