
#ifndef GDQ_RENDER_L_QNORMS_H
#define GDQ_RENDER_L_QNORMS_H

#include <gccore.h>

extern f32 MODEL_L_Q_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
