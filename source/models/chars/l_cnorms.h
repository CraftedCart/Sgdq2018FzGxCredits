
#ifndef GDQ_RENDER_L_CNORMS_H
#define GDQ_RENDER_L_CNORMS_H

#include <gccore.h>

extern f32 MODEL_L_C_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
