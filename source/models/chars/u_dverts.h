
#ifndef GDQ_RENDER_U_DVERTS_H
#define GDQ_RENDER_U_DVERTS_H

#include <gccore.h>

extern f32 MODEL_U_D_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_U_D_TRIS 48

#endif
