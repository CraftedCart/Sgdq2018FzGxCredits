
#ifndef GDQ_RENDER_L_TTEXCOORDS_H
#define GDQ_RENDER_L_TTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_L_T_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
