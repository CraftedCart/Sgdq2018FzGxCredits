
#ifndef GDQ_RENDER_L_INORMS_H
#define GDQ_RENDER_L_INORMS_H

#include <gccore.h>

extern f32 MODEL_L_I_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
