
#ifndef GDQ_RENDER_U_PVERTS_H
#define GDQ_RENDER_U_PVERTS_H

#include <gccore.h>

extern f32 MODEL_U_P_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_U_P_TRIS 48

#endif
