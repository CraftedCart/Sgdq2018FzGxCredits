
#ifndef GDQ_RENDER_L_DVERTS_H
#define GDQ_RENDER_L_DVERTS_H

#include <gccore.h>

extern f32 MODEL_L_D_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_L_D_TRIS 52

#endif
