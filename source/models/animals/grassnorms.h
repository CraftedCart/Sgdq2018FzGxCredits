
#ifndef GDQ_RENDER_GRASSNORMS_H
#define GDQ_RENDER_GRASSNORMS_H

#include <gccore.h>

extern f32 MODEL_GRASS_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
