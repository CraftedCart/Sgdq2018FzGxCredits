
#ifndef GDQ_RENDER_TURRETSLASERKILLVERTS_H
#define GDQ_RENDER_TURRETSLASERKILLVERTS_H

#include <gccore.h>

extern f32 MODEL_TURRETSLASERKILL_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_TURRETSLASERKILL_TRIS 6

#endif
