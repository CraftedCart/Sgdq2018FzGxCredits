
#ifndef GDQ_RENDER_SMOKETEXCOORDS_H
#define GDQ_RENDER_SMOKETEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_SMOKE_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
