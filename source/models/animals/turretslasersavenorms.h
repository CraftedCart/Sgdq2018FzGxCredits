
#ifndef GDQ_RENDER_TURRETSLASERSAVENORMS_H
#define GDQ_RENDER_TURRETSLASERSAVENORMS_H

#include <gccore.h>

extern f32 MODEL_TURRETSLASERSAVE_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
