
#ifndef GDQ_RENDER_POWERBOXVERTS_H
#define GDQ_RENDER_POWERBOXVERTS_H

#include <gccore.h>

extern f32 MODEL_POWERBOX_VERTS[] ATTRIBUTE_ALIGN(32);
#define MODEL_POWERBOX_TRIS 10

#endif
