
#ifndef GDQ_RENDER_POWERBOXNORMS_H
#define GDQ_RENDER_POWERBOXNORMS_H

#include <gccore.h>

extern f32 MODEL_POWERBOX_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
