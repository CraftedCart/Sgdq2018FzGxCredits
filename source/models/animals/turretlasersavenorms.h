
#ifndef GDQ_RENDER_TURRETLASERSAVENORMS_H
#define GDQ_RENDER_TURRETLASERSAVENORMS_H

#include <gccore.h>

extern f32 MODEL_TURRETLASERSAVE_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
