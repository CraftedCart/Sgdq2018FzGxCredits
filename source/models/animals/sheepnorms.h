
#ifndef GDQ_RENDER_SHEEPNORMS_H
#define GDQ_RENDER_SHEEPNORMS_H

#include <gccore.h>

extern f32 MODEL_SHEEP_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
