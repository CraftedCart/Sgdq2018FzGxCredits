
#ifndef GDQ_RENDER_SMOKENORMS_H
#define GDQ_RENDER_SMOKENORMS_H

#include <gccore.h>

extern f32 MODEL_SMOKE_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
