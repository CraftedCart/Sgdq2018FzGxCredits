
#include "roomfloorsnorms.h"

f32 MODEL_ROOMFLOORS_NORMS[] ATTRIBUTE_ALIGN(32) = {
    0.0f, 0.8632082343101501f, 0.504848062992096f,    0.0f, 0.8632082343101501f, 0.504848062992096f,    0.0f, 0.8632082343101501f, 0.504848062992096f,
    0.0f, -0.0f, 1.0f,    0.0f, -0.0f, 1.0f,    0.0f, -0.0f, 1.0f,
    0.0f, 1.0f, 0.0f,    0.0f, 1.0f, 0.0f,    0.0f, 1.0f, 0.0f,
    -0.0f, 1.0f, -0.0f,    -0.0f, 1.0f, -0.0f,    -0.0f, 1.0f, -0.0f,
    -0.0f, 1.0f, -0.0f,    -0.0f, 1.0f, -0.0f,    -0.0f, 1.0f, -0.0f,
    -1.0f, -0.0f, 0.0f,    -1.0f, -0.0f, 0.0f,    -1.0f, -0.0f, 0.0f,
    1.0f, 0.0f, 0.0f,    1.0f, 0.0f, 0.0f,    1.0f, 0.0f, 0.0f,
    0.0f, 0.0f, -1.0f,    0.0f, 0.0f, -1.0f,    0.0f, 0.0f, -1.0f,
    -0.0f, 0.9999999403953552f, -0.0f,    -0.0f, 0.9999999403953552f, -0.0f,    -0.0f, 0.9999999403953552f, -0.0f,
    -0.0f, 1.0f, -0.0f,    -0.0f, 1.0f, -0.0f,    -0.0f, 1.0f, -0.0f,
    0.0f, 1.0f, 0.0f,    0.0f, 1.0f, 0.0f,    0.0f, 1.0f, 0.0f,
    -0.0f, 1.0f, -0.0f,    -0.0f, 1.0f, -0.0f,    -0.0f, 1.0f, -0.0f,
    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,
    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,
    0.0f, 0.8632082343101501f, 0.504848062992096f,    0.0f, 0.8632082343101501f, 0.504848062992096f,    0.0f, 0.8632082343101501f, 0.504848062992096f,
    -0.0f, -0.0f, 1.0f,    -0.0f, -0.0f, 1.0f,    -0.0f, -0.0f, 1.0f,
    0.0f, 1.0f, 0.0f,    0.0f, 1.0f, 0.0f,    0.0f, 1.0f, 0.0f,
    0.0f, 1.0f, 0.0f,    0.0f, 1.0f, 0.0f,    0.0f, 1.0f, 0.0f,
    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,
    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,
    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,
    -1.0f, 0.0f, -0.0f,    -1.0f, 0.0f, -0.0f,    -1.0f, 0.0f, -0.0f,
    1.0f, 0.0f, -0.0f,    1.0f, 0.0f, -0.0f,    1.0f, 0.0f, -0.0f,
    -0.0f, 0.0f, -1.0f,    -0.0f, 0.0f, -1.0f,    -0.0f, 0.0f, -1.0f,
    0.0f, 0.9999999403953552f, -0.0f,    0.0f, 0.9999999403953552f, -0.0f,    0.0f, 0.9999999403953552f, -0.0f,
    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,
    -0.0f, 1.0f, -0.0f,    -0.0f, 1.0f, -0.0f,    -0.0f, 1.0f, -0.0f,
    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,
    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,    0.0f, 1.0f, -0.0f,
    0.0f, 1.0f, 0.0f,    0.0f, 1.0f, 0.0f,    0.0f, 1.0f, 0.0f,

};
