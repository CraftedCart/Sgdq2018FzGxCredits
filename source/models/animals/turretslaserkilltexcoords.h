
#ifndef GDQ_RENDER_TURRETSLASERKILLTEXCOORDS_H
#define GDQ_RENDER_TURRETSLASERKILLTEXCOORDS_H

#include <gccore.h>

extern f32 MODEL_TURRETSLASERKILL_TEXCOORDS[] ATTRIBUTE_ALIGN(32);

#endif
