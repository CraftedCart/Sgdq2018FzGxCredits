
#ifndef GDQ_RENDER_POWERWIRESNORMS_H
#define GDQ_RENDER_POWERWIRESNORMS_H

#include <gccore.h>

extern f32 MODEL_POWERWIRES_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
