
#ifndef GDQ_RENDER_GDQ2NORMS_H
#define GDQ_RENDER_GDQ2NORMS_H

#include <gccore.h>

extern f32 MODEL_GDQ2_NORMS[] ATTRIBUTE_ALIGN(32);

#endif
