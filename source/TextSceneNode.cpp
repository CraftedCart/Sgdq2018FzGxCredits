#include "TextSceneNode.hpp"
#include "MeshSceneNode.hpp"
#include "RenderManager.hpp"

//Pure mess
#include "models/chars/l_anorms.h"
#include "models/chars/l_atexcoords.h"
#include "models/chars/l_averts.h"
#include "models/chars/l_bnorms.h"
#include "models/chars/l_btexcoords.h"
#include "models/chars/l_bverts.h"
#include "models/chars/l_cnorms.h"
#include "models/chars/l_ctexcoords.h"
#include "models/chars/l_cverts.h"
#include "models/chars/l_dnorms.h"
#include "models/chars/l_dtexcoords.h"
#include "models/chars/l_dverts.h"
#include "models/chars/l_enorms.h"
#include "models/chars/l_etexcoords.h"
#include "models/chars/l_everts.h"
#include "models/chars/l_fnorms.h"
#include "models/chars/l_ftexcoords.h"
#include "models/chars/l_fverts.h"
#include "models/chars/l_gnorms.h"
#include "models/chars/l_gtexcoords.h"
#include "models/chars/l_gverts.h"
#include "models/chars/l_hnorms.h"
#include "models/chars/l_htexcoords.h"
#include "models/chars/l_hverts.h"
#include "models/chars/l_inorms.h"
#include "models/chars/l_itexcoords.h"
#include "models/chars/l_iverts.h"
#include "models/chars/l_jnorms.h"
#include "models/chars/l_jtexcoords.h"
#include "models/chars/l_jverts.h"
#include "models/chars/l_knorms.h"
#include "models/chars/l_ktexcoords.h"
#include "models/chars/l_kverts.h"
#include "models/chars/l_lnorms.h"
#include "models/chars/l_ltexcoords.h"
#include "models/chars/l_lverts.h"
#include "models/chars/l_mnorms.h"
#include "models/chars/l_mtexcoords.h"
#include "models/chars/l_mverts.h"
#include "models/chars/l_nnorms.h"
#include "models/chars/l_ntexcoords.h"
#include "models/chars/l_nverts.h"
#include "models/chars/l_onorms.h"
#include "models/chars/l_otexcoords.h"
#include "models/chars/l_overts.h"
#include "models/chars/l_pnorms.h"
#include "models/chars/l_ptexcoords.h"
#include "models/chars/l_pverts.h"
#include "models/chars/l_qnorms.h"
#include "models/chars/l_qtexcoords.h"
#include "models/chars/l_qverts.h"
#include "models/chars/l_rnorms.h"
#include "models/chars/l_rtexcoords.h"
#include "models/chars/l_rverts.h"
#include "models/chars/l_snorms.h"
#include "models/chars/l_stexcoords.h"
#include "models/chars/l_sverts.h"
#include "models/chars/l_tnorms.h"
#include "models/chars/l_ttexcoords.h"
#include "models/chars/l_tverts.h"
#include "models/chars/l_unorms.h"
#include "models/chars/l_utexcoords.h"
#include "models/chars/l_uverts.h"
#include "models/chars/l_vnorms.h"
#include "models/chars/l_vtexcoords.h"
#include "models/chars/l_vverts.h"
#include "models/chars/l_wnorms.h"
#include "models/chars/l_wtexcoords.h"
#include "models/chars/l_wverts.h"
#include "models/chars/l_xnorms.h"
#include "models/chars/l_xtexcoords.h"
#include "models/chars/l_xverts.h"
#include "models/chars/l_ynorms.h"
#include "models/chars/l_ytexcoords.h"
#include "models/chars/l_yverts.h"
#include "models/chars/l_znorms.h"
#include "models/chars/l_ztexcoords.h"
#include "models/chars/l_zverts.h"
#include "models/chars/u_anorms.h"
#include "models/chars/u_atexcoords.h"
#include "models/chars/u_averts.h"
#include "models/chars/u_bnorms.h"
#include "models/chars/u_btexcoords.h"
#include "models/chars/u_bverts.h"
#include "models/chars/u_cnorms.h"
#include "models/chars/u_ctexcoords.h"
#include "models/chars/u_cverts.h"
#include "models/chars/u_dnorms.h"
#include "models/chars/u_dtexcoords.h"
#include "models/chars/u_dverts.h"
#include "models/chars/u_enorms.h"
#include "models/chars/u_etexcoords.h"
#include "models/chars/u_everts.h"
#include "models/chars/u_fnorms.h"
#include "models/chars/u_ftexcoords.h"
#include "models/chars/u_fverts.h"
#include "models/chars/u_gnorms.h"
#include "models/chars/u_gtexcoords.h"
#include "models/chars/u_gverts.h"
#include "models/chars/u_hnorms.h"
#include "models/chars/u_htexcoords.h"
#include "models/chars/u_hverts.h"
#include "models/chars/u_inorms.h"
#include "models/chars/u_itexcoords.h"
#include "models/chars/u_iverts.h"
#include "models/chars/u_jnorms.h"
#include "models/chars/u_jtexcoords.h"
#include "models/chars/u_jverts.h"
#include "models/chars/u_knorms.h"
#include "models/chars/u_ktexcoords.h"
#include "models/chars/u_kverts.h"
#include "models/chars/u_lnorms.h"
#include "models/chars/u_ltexcoords.h"
#include "models/chars/u_lverts.h"
#include "models/chars/u_mnorms.h"
#include "models/chars/u_mtexcoords.h"
#include "models/chars/u_mverts.h"
#include "models/chars/u_nnorms.h"
#include "models/chars/u_ntexcoords.h"
#include "models/chars/u_nverts.h"
#include "models/chars/u_onorms.h"
#include "models/chars/u_otexcoords.h"
#include "models/chars/u_overts.h"
#include "models/chars/u_pnorms.h"
#include "models/chars/u_ptexcoords.h"
#include "models/chars/u_pverts.h"
#include "models/chars/u_qnorms.h"
#include "models/chars/u_qtexcoords.h"
#include "models/chars/u_qverts.h"
#include "models/chars/u_rnorms.h"
#include "models/chars/u_rtexcoords.h"
#include "models/chars/u_rverts.h"
#include "models/chars/u_snorms.h"
#include "models/chars/u_stexcoords.h"
#include "models/chars/u_sverts.h"
#include "models/chars/u_tnorms.h"
#include "models/chars/u_ttexcoords.h"
#include "models/chars/u_tverts.h"
#include "models/chars/u_unorms.h"
#include "models/chars/u_utexcoords.h"
#include "models/chars/u_uverts.h"
#include "models/chars/u_vnorms.h"
#include "models/chars/u_vtexcoords.h"
#include "models/chars/u_vverts.h"
#include "models/chars/u_wnorms.h"
#include "models/chars/u_wtexcoords.h"
#include "models/chars/u_wverts.h"
#include "models/chars/u_xnorms.h"
#include "models/chars/u_xtexcoords.h"
#include "models/chars/u_xverts.h"
#include "models/chars/u_ynorms.h"
#include "models/chars/u_ytexcoords.h"
#include "models/chars/u_yverts.h"
#include "models/chars/u_znorms.h"
#include "models/chars/u_ztexcoords.h"
#include "models/chars/u_zverts.h"
#include "models/chars/n_0norms.h"
#include "models/chars/n_0texcoords.h"
#include "models/chars/n_0verts.h"
#include "models/chars/n_1norms.h"
#include "models/chars/n_1texcoords.h"
#include "models/chars/n_1verts.h"
#include "models/chars/n_2norms.h"
#include "models/chars/n_2texcoords.h"
#include "models/chars/n_2verts.h"
#include "models/chars/n_3norms.h"
#include "models/chars/n_3texcoords.h"
#include "models/chars/n_3verts.h"
#include "models/chars/n_4norms.h"
#include "models/chars/n_4texcoords.h"
#include "models/chars/n_4verts.h"
#include "models/chars/n_5norms.h"
#include "models/chars/n_5texcoords.h"
#include "models/chars/n_5verts.h"
#include "models/chars/n_6norms.h"
#include "models/chars/n_6texcoords.h"
#include "models/chars/n_6verts.h"
#include "models/chars/n_7norms.h"
#include "models/chars/n_7texcoords.h"
#include "models/chars/n_7verts.h"
#include "models/chars/n_8norms.h"
#include "models/chars/n_8texcoords.h"
#include "models/chars/n_8verts.h"
#include "models/chars/n_9norms.h"
#include "models/chars/n_9texcoords.h"
#include "models/chars/n_9verts.h"
#include "models/chars/s_dashnorms.h"
#include "models/chars/s_dashtexcoords.h"
#include "models/chars/s_dashverts.h"

//Forward declarations
namespace GdqRender {
    f32 lerp(const f32 t, const f32 a, const f32 b);
}

//Define statics
std::unordered_map<u32, f32*> TextSceneNode::charVerts;
std::unordered_map<u32, f32*> TextSceneNode::charNorms;
std::unordered_map<u32, f32*> TextSceneNode::charUvs;
std::unordered_map<u32, u32> TextSceneNode::charTris;

void TextSceneNode::staticInit() {
    //Uppercase mess
    charVerts[ 65] = MODEL_U_A_VERTS;
    charNorms[ 65] = MODEL_U_A_NORMS;
    charUvs  [ 65] = MODEL_U_A_TEXCOORDS;
    charTris [ 65] = MODEL_U_A_TRIS;
    charVerts[ 66] = MODEL_U_B_VERTS;
    charNorms[ 66] = MODEL_U_B_NORMS;
    charUvs  [ 66] = MODEL_U_B_TEXCOORDS;
    charTris [ 66] = MODEL_U_B_TRIS;
    charVerts[ 67] = MODEL_U_C_VERTS;
    charNorms[ 67] = MODEL_U_C_NORMS;
    charUvs  [ 67] = MODEL_U_C_TEXCOORDS;
    charTris [ 67] = MODEL_U_C_TRIS;
    charVerts[ 68] = MODEL_U_D_VERTS;
    charNorms[ 68] = MODEL_U_D_NORMS;
    charUvs  [ 68] = MODEL_U_D_TEXCOORDS;
    charTris [ 68] = MODEL_U_D_TRIS;
    charVerts[ 69] = MODEL_U_E_VERTS;
    charNorms[ 69] = MODEL_U_E_NORMS;
    charUvs  [ 69] = MODEL_U_E_TEXCOORDS;
    charTris [ 69] = MODEL_U_E_TRIS;
    charVerts[ 70] = MODEL_U_F_VERTS;
    charNorms[ 70] = MODEL_U_F_NORMS;
    charUvs  [ 70] = MODEL_U_F_TEXCOORDS;
    charTris [ 70] = MODEL_U_F_TRIS;
    charVerts[ 71] = MODEL_U_G_VERTS;
    charNorms[ 71] = MODEL_U_G_NORMS;
    charUvs  [ 71] = MODEL_U_G_TEXCOORDS;
    charTris [ 71] = MODEL_U_G_TRIS;
    charVerts[ 72] = MODEL_U_H_VERTS;
    charNorms[ 72] = MODEL_U_H_NORMS;
    charUvs  [ 72] = MODEL_U_H_TEXCOORDS;
    charTris [ 72] = MODEL_U_H_TRIS;
    charVerts[ 73] = MODEL_U_I_VERTS;
    charNorms[ 73] = MODEL_U_I_NORMS;
    charUvs  [ 73] = MODEL_U_I_TEXCOORDS;
    charTris [ 73] = MODEL_U_I_TRIS;
    charVerts[ 74] = MODEL_U_J_VERTS;
    charNorms[ 74] = MODEL_U_J_NORMS;
    charUvs  [ 74] = MODEL_U_J_TEXCOORDS;
    charTris [ 74] = MODEL_U_J_TRIS;
    charVerts[ 75] = MODEL_U_K_VERTS;
    charNorms[ 75] = MODEL_U_K_NORMS;
    charUvs  [ 75] = MODEL_U_K_TEXCOORDS;
    charTris [ 75] = MODEL_U_K_TRIS;
    charVerts[ 76] = MODEL_U_L_VERTS;
    charNorms[ 76] = MODEL_U_L_NORMS;
    charUvs  [ 76] = MODEL_U_L_TEXCOORDS;
    charTris [ 76] = MODEL_U_L_TRIS;
    charVerts[ 77] = MODEL_U_M_VERTS;
    charNorms[ 77] = MODEL_U_M_NORMS;
    charUvs  [ 77] = MODEL_U_M_TEXCOORDS;
    charTris [ 77] = MODEL_U_M_TRIS;
    charVerts[ 78] = MODEL_U_N_VERTS;
    charNorms[ 78] = MODEL_U_N_NORMS;
    charUvs  [ 78] = MODEL_U_N_TEXCOORDS;
    charTris [ 78] = MODEL_U_N_TRIS;
    charVerts[ 79] = MODEL_U_O_VERTS;
    charNorms[ 79] = MODEL_U_O_NORMS;
    charUvs  [ 79] = MODEL_U_O_TEXCOORDS;
    charTris [ 79] = MODEL_U_O_TRIS;
    charVerts[ 80] = MODEL_U_P_VERTS;
    charNorms[ 80] = MODEL_U_P_NORMS;
    charUvs  [ 80] = MODEL_U_P_TEXCOORDS;
    charTris [ 80] = MODEL_U_P_TRIS;
    charVerts[ 81] = MODEL_U_Q_VERTS;
    charNorms[ 81] = MODEL_U_Q_NORMS;
    charUvs  [ 81] = MODEL_U_Q_TEXCOORDS;
    charTris [ 81] = MODEL_U_Q_TRIS;
    charVerts[ 82] = MODEL_U_R_VERTS;
    charNorms[ 82] = MODEL_U_R_NORMS;
    charUvs  [ 82] = MODEL_U_R_TEXCOORDS;
    charTris [ 82] = MODEL_U_R_TRIS;
    charVerts[ 83] = MODEL_U_S_VERTS;
    charNorms[ 83] = MODEL_U_S_NORMS;
    charUvs  [ 83] = MODEL_U_S_TEXCOORDS;
    charTris [ 83] = MODEL_U_S_TRIS;
    charVerts[ 84] = MODEL_U_T_VERTS;
    charNorms[ 84] = MODEL_U_T_NORMS;
    charUvs  [ 84] = MODEL_U_T_TEXCOORDS;
    charTris [ 84] = MODEL_U_T_TRIS;
    charVerts[ 85] = MODEL_U_U_VERTS;
    charNorms[ 85] = MODEL_U_U_NORMS;
    charUvs  [ 85] = MODEL_U_U_TEXCOORDS;
    charTris [ 85] = MODEL_U_U_TRIS;
    charVerts[ 86] = MODEL_U_V_VERTS;
    charNorms[ 86] = MODEL_U_V_NORMS;
    charUvs  [ 86] = MODEL_U_V_TEXCOORDS;
    charTris [ 86] = MODEL_U_V_TRIS;
    charVerts[ 87] = MODEL_U_W_VERTS;
    charNorms[ 87] = MODEL_U_W_NORMS;
    charUvs  [ 87] = MODEL_U_W_TEXCOORDS;
    charTris [ 87] = MODEL_U_W_TRIS;
    charVerts[ 88] = MODEL_U_X_VERTS;
    charNorms[ 88] = MODEL_U_X_NORMS;
    charUvs  [ 88] = MODEL_U_X_TEXCOORDS;
    charTris [ 88] = MODEL_U_X_TRIS;
    charVerts[ 89] = MODEL_U_Y_VERTS;
    charNorms[ 89] = MODEL_U_Y_NORMS;
    charUvs  [ 89] = MODEL_U_Y_TEXCOORDS;
    charTris [ 89] = MODEL_U_Y_TRIS;
    charVerts[ 90] = MODEL_U_Z_VERTS;
    charNorms[ 90] = MODEL_U_Z_NORMS;
    charUvs  [ 90] = MODEL_U_Z_TEXCOORDS;
    charTris [ 90] = MODEL_U_Z_TRIS;

    //Lowercase mess
    charVerts[ 97] = MODEL_L_A_VERTS;
    charNorms[ 97] = MODEL_L_A_NORMS;
    charUvs  [ 97] = MODEL_L_A_TEXCOORDS;
    charTris [ 97] = MODEL_L_A_TRIS;
    charVerts[ 98] = MODEL_L_B_VERTS;
    charNorms[ 98] = MODEL_L_B_NORMS;
    charUvs  [ 98] = MODEL_L_B_TEXCOORDS;
    charTris [ 98] = MODEL_L_B_TRIS;
    charVerts[ 99] = MODEL_L_C_VERTS;
    charNorms[ 99] = MODEL_L_C_NORMS;
    charUvs  [ 99] = MODEL_L_C_TEXCOORDS;
    charTris [ 99] = MODEL_L_C_TRIS;
    charVerts[100] = MODEL_L_D_VERTS;
    charNorms[100] = MODEL_L_D_NORMS;
    charUvs  [100] = MODEL_L_D_TEXCOORDS;
    charTris [100] = MODEL_L_D_TRIS;
    charVerts[101] = MODEL_L_E_VERTS;
    charNorms[101] = MODEL_L_E_NORMS;
    charUvs  [101] = MODEL_L_E_TEXCOORDS;
    charTris [101] = MODEL_L_E_TRIS;
    charVerts[102] = MODEL_L_F_VERTS;
    charNorms[102] = MODEL_L_F_NORMS;
    charUvs  [102] = MODEL_L_F_TEXCOORDS;
    charTris [102] = MODEL_L_F_TRIS;
    charVerts[103] = MODEL_L_G_VERTS;
    charNorms[103] = MODEL_L_G_NORMS;
    charUvs  [103] = MODEL_L_G_TEXCOORDS;
    charTris [103] = MODEL_L_G_TRIS;
    charVerts[104] = MODEL_L_H_VERTS;
    charNorms[104] = MODEL_L_H_NORMS;
    charUvs  [104] = MODEL_L_H_TEXCOORDS;
    charTris [104] = MODEL_L_H_TRIS;
    charVerts[105] = MODEL_L_I_VERTS;
    charNorms[105] = MODEL_L_I_NORMS;
    charUvs  [105] = MODEL_L_I_TEXCOORDS;
    charTris [105] = MODEL_L_I_TRIS;
    charVerts[106] = MODEL_L_J_VERTS;
    charNorms[106] = MODEL_L_J_NORMS;
    charUvs  [106] = MODEL_L_J_TEXCOORDS;
    charTris [106] = MODEL_L_J_TRIS;
    charVerts[107] = MODEL_L_K_VERTS;
    charNorms[107] = MODEL_L_K_NORMS;
    charUvs  [107] = MODEL_L_K_TEXCOORDS;
    charTris [107] = MODEL_L_K_TRIS;
    charVerts[108] = MODEL_L_L_VERTS;
    charNorms[108] = MODEL_L_L_NORMS;
    charUvs  [108] = MODEL_L_L_TEXCOORDS;
    charTris [108] = MODEL_L_L_TRIS;
    charVerts[109] = MODEL_L_M_VERTS;
    charNorms[109] = MODEL_L_M_NORMS;
    charUvs  [109] = MODEL_L_M_TEXCOORDS;
    charTris [109] = MODEL_L_M_TRIS;
    charVerts[110] = MODEL_L_N_VERTS;
    charNorms[110] = MODEL_L_N_NORMS;
    charUvs  [110] = MODEL_L_N_TEXCOORDS;
    charTris [110] = MODEL_L_N_TRIS;
    charVerts[111] = MODEL_L_O_VERTS;
    charNorms[111] = MODEL_L_O_NORMS;
    charUvs  [111] = MODEL_L_O_TEXCOORDS;
    charTris [111] = MODEL_L_O_TRIS;
    charVerts[112] = MODEL_L_P_VERTS;
    charNorms[112] = MODEL_L_P_NORMS;
    charUvs  [112] = MODEL_L_P_TEXCOORDS;
    charTris [112] = MODEL_L_P_TRIS;
    charVerts[113] = MODEL_L_Q_VERTS;
    charNorms[113] = MODEL_L_Q_NORMS;
    charUvs  [113] = MODEL_L_Q_TEXCOORDS;
    charTris [113] = MODEL_L_Q_TRIS;
    charVerts[114] = MODEL_L_R_VERTS;
    charNorms[114] = MODEL_L_R_NORMS;
    charUvs  [114] = MODEL_L_R_TEXCOORDS;
    charTris [114] = MODEL_L_R_TRIS;
    charVerts[115] = MODEL_L_S_VERTS;
    charNorms[115] = MODEL_L_S_NORMS;
    charUvs  [115] = MODEL_L_S_TEXCOORDS;
    charTris [115] = MODEL_L_S_TRIS;
    charVerts[116] = MODEL_L_T_VERTS;
    charNorms[116] = MODEL_L_T_NORMS;
    charUvs  [116] = MODEL_L_T_TEXCOORDS;
    charTris [116] = MODEL_L_T_TRIS;
    charVerts[117] = MODEL_L_U_VERTS;
    charNorms[117] = MODEL_L_U_NORMS;
    charUvs  [117] = MODEL_L_U_TEXCOORDS;
    charTris [117] = MODEL_L_U_TRIS;
    charVerts[118] = MODEL_L_V_VERTS;
    charNorms[118] = MODEL_L_V_NORMS;
    charUvs  [118] = MODEL_L_V_TEXCOORDS;
    charTris [118] = MODEL_L_V_TRIS;
    charVerts[119] = MODEL_L_W_VERTS;
    charNorms[119] = MODEL_L_W_NORMS;
    charUvs  [119] = MODEL_L_W_TEXCOORDS;
    charTris [119] = MODEL_L_W_TRIS;
    charVerts[120] = MODEL_L_X_VERTS;
    charNorms[120] = MODEL_L_X_NORMS;
    charUvs  [120] = MODEL_L_X_TEXCOORDS;
    charTris [120] = MODEL_L_X_TRIS;
    charVerts[121] = MODEL_L_Y_VERTS;
    charNorms[121] = MODEL_L_Y_NORMS;
    charUvs  [121] = MODEL_L_Y_TEXCOORDS;
    charTris [121] = MODEL_L_Y_TRIS;
    charVerts[122] = MODEL_L_Z_VERTS;
    charNorms[122] = MODEL_L_Z_NORMS;
    charUvs  [122] = MODEL_L_Z_TEXCOORDS;
    charTris [122] = MODEL_L_Z_TRIS;

    //Numbers mess
    charVerts[ 48] = MODEL_N_0_VERTS;
    charNorms[ 48] = MODEL_N_0_NORMS;
    charUvs  [ 48] = MODEL_N_0_TEXCOORDS;
    charTris [ 48] = MODEL_N_0_TRIS;
    charVerts[ 49] = MODEL_N_1_VERTS;
    charNorms[ 49] = MODEL_N_1_NORMS;
    charUvs  [ 49] = MODEL_N_1_TEXCOORDS;
    charTris [ 49] = MODEL_N_1_TRIS;
    charVerts[ 50] = MODEL_N_2_VERTS;
    charNorms[ 50] = MODEL_N_2_NORMS;
    charUvs  [ 50] = MODEL_N_2_TEXCOORDS;
    charTris [ 50] = MODEL_N_2_TRIS;
    charVerts[ 51] = MODEL_N_3_VERTS;
    charNorms[ 51] = MODEL_N_3_NORMS;
    charUvs  [ 51] = MODEL_N_3_TEXCOORDS;
    charTris [ 51] = MODEL_N_3_TRIS;
    charVerts[ 52] = MODEL_N_4_VERTS;
    charNorms[ 52] = MODEL_N_4_NORMS;
    charUvs  [ 52] = MODEL_N_4_TEXCOORDS;
    charTris [ 52] = MODEL_N_4_TRIS;
    charVerts[ 53] = MODEL_N_5_VERTS;
    charNorms[ 53] = MODEL_N_5_NORMS;
    charUvs  [ 53] = MODEL_N_5_TEXCOORDS;
    charTris [ 53] = MODEL_N_5_TRIS;
    charVerts[ 54] = MODEL_N_6_VERTS;
    charNorms[ 54] = MODEL_N_6_NORMS;
    charUvs  [ 54] = MODEL_N_6_TEXCOORDS;
    charTris [ 54] = MODEL_N_6_TRIS;
    charVerts[ 55] = MODEL_N_7_VERTS;
    charNorms[ 55] = MODEL_N_7_NORMS;
    charUvs  [ 55] = MODEL_N_7_TEXCOORDS;
    charTris [ 55] = MODEL_N_7_TRIS;
    charVerts[ 56] = MODEL_N_8_VERTS;
    charNorms[ 56] = MODEL_N_8_NORMS;
    charUvs  [ 56] = MODEL_N_8_TEXCOORDS;
    charTris [ 56] = MODEL_N_8_TRIS;
    charVerts[ 57] = MODEL_N_9_VERTS;
    charNorms[ 57] = MODEL_N_9_NORMS;
    charUvs  [ 57] = MODEL_N_9_TEXCOORDS;
    charTris [ 57] = MODEL_N_9_TRIS;

    //Symbols mess
    charVerts[ 45] = MODEL_S_DASH_VERTS;
    charNorms[ 45] = MODEL_S_DASH_NORMS;
    charUvs  [ 45] = MODEL_S_DASH_TEXCOORDS;
    charTris [ 45] = MODEL_S_DASH_TRIS;
}

void TextSceneNode::tick() {
    for (SceneNode *node : children) {
        node->getTransform().pos.z = GdqRender::lerp(0.02f, node->getTransform().pos.z, 0.0f);
        node->getTransform().rot.z = GdqRender::lerp(0.02f, node->getTransform().rot.z, 0.0f);
    }

    if (disperse) {
        getTransform().scl.y = GdqRender::lerp(0.1f, getTransform().scl.y, 0.0f);
        getTransform().scl.z = GdqRender::lerp(0.1f, getTransform().scl.z, 0.0f);
    }
}

void TextSceneNode::spawnText(const char *text) {
    u32 i = 0;
    char p = text[0];
    while (p != '\0') {
        MeshSceneNode *mesh = new MeshSceneNode();
        mesh->meshVertices = charVerts[p];
        mesh->meshNormals = charNorms[p];
        mesh->meshUvs = charUvs[p];
        mesh->triangleCount = charTris[p];
        mesh->texture = &RenderManager::pallete2Tex;
        mesh->getTransform().pos.x = i * 17.0f;
        mesh->getTransform().pos.z = (i + 1) * -1000.0f;
        mesh->getTransform().rot.z = (i + 8) * M_PI;
        addChild(mesh);

        ++i;
        p = text[i];
    }
}

