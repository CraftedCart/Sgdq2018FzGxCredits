#ifndef GDQ_RENDER_CARDMANAGER_HPP
#define GDQ_RENDER_CARDMANAGER_HPP

#include <gccore.h>

namespace CardManager {
    extern unsigned char workArea[CARD_WORKAREA] ATTRIBUTE_ALIGN(32);

    int init();

    /**
     * @brief This crashes and I don't exactly know why so don't use this ok
     *
     * @param fileName
     *
     * @return
     */
    void* openFile(const char *fileName, int *outSize);

    int roundUp(int numToRound, int multiple);
}

#endif

