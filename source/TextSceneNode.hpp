#ifndef GDQ_RENDER_TEXTSCENENODE_HPP
#define GDQ_RENDER_TEXTSCENENODE_HPP

#include "SceneNode.hpp"
#include <unordered_map>

class TextSceneNode : public SceneNode {
    public:
        static std::unordered_map<u32, f32*> charVerts;
        static std::unordered_map<u32, f32*> charNorms;
        static std::unordered_map<u32, f32*> charUvs;
        static std::unordered_map<u32, u32> charTris;

        bool disperse = false;

    public:
        static void staticInit();

        virtual void tick() override;
        void spawnText(const char *text);
};

#endif

