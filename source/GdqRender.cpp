#include "GdqRender.hpp"
#include "AnimalsRender.hpp"
#include "PreAnimalsRender.hpp"
#include "MetroidRender.hpp"
#include "CardManager.hpp"
#include "verts.h"
#include "norms.h"
//#include "colors.h"
#include "texcoords.h"
#include "pallete_tpl.h"
#include "pallete.h"
#include "models/gdq2verts.h"
#include "models/gdq2norms.h"
#include "models/gdq2texcoords.h"
#include "pallete2_tpl.h"
#include "pallete2.h"
#include "models/spaceverts.h"
#include "models/spacenorms.h"
#include "models/spacetexcoords.h"
#include "spacetex_tpl.h"
#include "spacetex.h"
#include "models/billboardverts.h"
#include "models/billboardnorms.h"
#include "models/billboardtexcoords.h"
#include "kappa_tpl.h"
#include "kappa.h"
#include "models/outertubeverts.h"
#include "models/outertubenorms.h"
#include "models/outertubetexcoords.h"
#include "models/tubecircuitsverts.h"
#include "models/tubecircuitsnorms.h"
#include "models/tubecircuitstexcoords.h"
#include "neogrid_tpl.h"
#include "neogrid.h"
#include "corrupt_tpl.h"
#include "corrupt.h"
#include "models/creditshereverts.h"
#include "models/creditsherenorms.h"
#include "models/creditsheretexcoords.h"
#include "animaltex1_tpl.h"
#include "animaltex1.h"
#include "animaltex2_tpl.h"
#include "animaltex2.h"
#include "animaltex3_tpl.h"
#include "animaltex3.h"
#include "animaltex4_tpl.h"
#include "animaltex4.h"
#include "animaltex5_tpl.h"
#include "animaltex5.h"
#include "animaltex6_tpl.h"
#include "animaltex6.h"
#include "animaltex7_tpl.h"
#include "animaltex7.h"
#include "animaltex8_tpl.h"
#include "animaltex8.h"
#include "portalgun_tpl.h"
#include "portalgun.h"
#include "portalb1_tpl.h"
#include "portalb1.h"
#include "portalb2_tpl.h"
#include "portalb2.h"
#include "portalb3_tpl.h"
#include "portalb3.h"
#include "portalo1_tpl.h"
#include "portalo1.h"
#include "portalo2_tpl.h"
#include "portalo2.h"
#include "portalo3_tpl.h"
#include "portalo3.h"
#include "endkill_tpl.h"
#include "endkill.h"
#include "endsave_tpl.h"
#include "endsave.h"
#include "MeshSceneNode.hpp"
#include "EmitterSceneNode.hpp"
#include "TextSceneNode.hpp"
#include "RenderManager.hpp"
//#include "theredone_bin.h"
#include <gccore.h>
#include <asndlib.h>
#include <mp3player.h>
#include <aesndlib.h>
#include <asndlib.h>
//#include <gcmodplay.h>
#include <algorithm>
#include <cstdio>
#include <unistd.h>

namespace GdqRender {
    SceneNode *rootNode;
    PADStatus pads[4];

    int execute() {
        RenderManager::backgroundColor = {0, 0, 0, 255};
        RenderManager::init();
        SceneNode node;
        RenderManager::draw(&node);
        //MODPlay play;

        //Initialise the audio subsystem
        //AESND_Init();
        ASND_Init();

        //Static initialize text scene node
        TextSceneNode::staticInit();

        //Set up the scene graph
        rootNode = new SceneNode();

        // bool isBlue = true;
        // bool bDown = false;
        //Pause screen
        // while (1) {
        //     RenderManager::draw(rootNode);

        //     PAD_Read(pads);
        //     if (pads[0].button & PAD_BUTTON_A) {
        //         break;
        //     }

        //     if (pads[0].button & PAD_BUTTON_B && !bDown) {
        //         bDown = true;
        //         if (isBlue) {
        //             RenderManager::backgroundColor = {0, 255, 0, 255};
        //         } else {
        //             RenderManager::backgroundColor = {0, 0, 255, 255};
        //         }
        //         isBlue = !isBlue;
        //     }

        //     if (!(pads[0].button & PAD_BUTTON_B)) {
        //         bDown = false;
        //     }
        // }

        //MODPlay_Init(&play);
        //MODPlay_SetMOD(&play, theredone_bin);
        //MODPlay_Start(&play);

        //Init memcard stuff
        CardManager::init();
        int size;
        void *musicBuffer = CardManager::openFile("fzgxmusic", &size);

        MP3Player_Init();

        //Load in textures
        RenderManager::pallete2Tex = RenderManager::loadTplTextureFromMemory(pallete2_tpl, pallete2_tpl_size, pallete2);
        GXTexObj spaceTex = RenderManager::loadTplTextureFromMemory(spacetex_tpl, spacetex_tpl_size, spacetex);
        GXTexObj kappaTex = RenderManager::loadTplTextureFromMemory(kappa_tpl, kappa_tpl_size, kappa);
        GXTexObj neogridTex = RenderManager::loadTplTextureFromMemory(neogrid_tpl, neogrid_tpl_size, neogrid);
        GXTexObj corruptTex = RenderManager::loadTplTextureFromMemory(corrupt_tpl, corrupt_tpl_size, corrupt);

        RenderManager::animalTex1 = RenderManager::loadTplTextureFromMemory(animaltex1_tpl, animaltex1_tpl_size, animaltex1);
        GX_InitTexObjLOD(&RenderManager::animalTex1, GX_NEAR, GX_NEAR, 0.0f, 1.0f, 0.0f, GX_DISABLE, GX_DISABLE, GX_ANISO_1);
        RenderManager::animalTex2 = RenderManager::loadTplTextureFromMemory(animaltex2_tpl, animaltex2_tpl_size, animaltex2);
        GX_InitTexObjLOD(&RenderManager::animalTex2, GX_NEAR, GX_NEAR, 0.0f, 1.0f, 0.0f, GX_DISABLE, GX_DISABLE, GX_ANISO_1);
        RenderManager::animalTex3 = RenderManager::loadTplTextureFromMemory(animaltex3_tpl, animaltex3_tpl_size, animaltex3);
        GX_InitTexObjLOD(&RenderManager::animalTex3, GX_NEAR, GX_NEAR, 0.0f, 1.0f, 0.0f, GX_DISABLE, GX_DISABLE, GX_ANISO_1);
        RenderManager::animalTex4 = RenderManager::loadTplTextureFromMemory(animaltex4_tpl, animaltex4_tpl_size, animaltex4);
        GX_InitTexObjLOD(&RenderManager::animalTex4, GX_NEAR, GX_NEAR, 0.0f, 1.0f, 0.0f, GX_DISABLE, GX_DISABLE, GX_ANISO_1);
        RenderManager::animalTex5 = RenderManager::loadTplTextureFromMemory(animaltex5_tpl, animaltex5_tpl_size, animaltex5);
        GX_InitTexObjLOD(&RenderManager::animalTex5, GX_NEAR, GX_NEAR, 0.0f, 1.0f, 0.0f, GX_DISABLE, GX_DISABLE, GX_ANISO_1);
        RenderManager::animalTex6 = RenderManager::loadTplTextureFromMemory(animaltex6_tpl, animaltex6_tpl_size, animaltex6);
        GX_InitTexObjLOD(&RenderManager::animalTex6, GX_NEAR, GX_NEAR, 0.0f, 1.0f, 0.0f, GX_DISABLE, GX_DISABLE, GX_ANISO_1);
        RenderManager::animalTex7 = RenderManager::loadTplTextureFromMemory(animaltex7_tpl, animaltex7_tpl_size, animaltex7);
        GX_InitTexObjLOD(&RenderManager::animalTex7, GX_NEAR, GX_NEAR, 0.0f, 1.0f, 0.0f, GX_DISABLE, GX_DISABLE, GX_ANISO_1);
        RenderManager::animalTex8 = RenderManager::loadTplTextureFromMemory(animaltex8_tpl, animaltex8_tpl_size, animaltex8);
        GX_InitTexObjLOD(&RenderManager::animalTex8, GX_NEAR, GX_NEAR, 0.0f, 1.0f, 0.0f, GX_DISABLE, GX_DISABLE, GX_ANISO_1);
        RenderManager::portalGunTex = RenderManager::loadTplTextureFromMemory(portalgun_tpl, portalgun_tpl_size, portalgun);
        GX_InitTexObjLOD(&RenderManager::portalGunTex, GX_NEAR, GX_NEAR, 0.0f, 1.0f, 0.0f, GX_DISABLE, GX_DISABLE, GX_ANISO_1);
        RenderManager::portalB1Tex = RenderManager::loadTplTextureFromMemory(portalb1_tpl, portalb1_tpl_size, portalb1);
        GX_InitTexObjLOD(&RenderManager::portalB1Tex, GX_NEAR, GX_NEAR, 0.0f, 1.0f, 0.0f, GX_DISABLE, GX_DISABLE, GX_ANISO_1);
        RenderManager::portalB2Tex = RenderManager::loadTplTextureFromMemory(portalb2_tpl, portalb2_tpl_size, portalb2);
        GX_InitTexObjLOD(&RenderManager::portalB2Tex, GX_NEAR, GX_NEAR, 0.0f, 1.0f, 0.0f, GX_DISABLE, GX_DISABLE, GX_ANISO_1);
        RenderManager::portalB3Tex = RenderManager::loadTplTextureFromMemory(portalb3_tpl, portalb3_tpl_size, portalb3);
        GX_InitTexObjLOD(&RenderManager::portalB3Tex, GX_NEAR, GX_NEAR, 0.0f, 1.0f, 0.0f, GX_DISABLE, GX_DISABLE, GX_ANISO_1);
        RenderManager::portalO1Tex = RenderManager::loadTplTextureFromMemory(portalo1_tpl, portalo1_tpl_size, portalo1);
        GX_InitTexObjLOD(&RenderManager::portalO1Tex, GX_NEAR, GX_NEAR, 0.0f, 1.0f, 0.0f, GX_DISABLE, GX_DISABLE, GX_ANISO_1);
        RenderManager::portalO2Tex = RenderManager::loadTplTextureFromMemory(portalo2_tpl, portalo2_tpl_size, portalo2);
        GX_InitTexObjLOD(&RenderManager::portalO2Tex, GX_NEAR, GX_NEAR, 0.0f, 1.0f, 0.0f, GX_DISABLE, GX_DISABLE, GX_ANISO_1);
        RenderManager::portalO3Tex = RenderManager::loadTplTextureFromMemory(portalo3_tpl, portalo3_tpl_size, portalo3);
        GX_InitTexObjLOD(&RenderManager::portalO3Tex, GX_NEAR, GX_NEAR, 0.0f, 1.0f, 0.0f, GX_DISABLE, GX_DISABLE, GX_ANISO_1);

        u32 frameTime = 0;

        PreAnimalsRender::execute();
        MetroidRender::execute();
        AnimalsRender::execute();

        //Reset the camera
        RenderManager::camera = {0.0f, 0.0f, 50.0f};
        RenderManager::up = {0.0f, 1.0f, 0.0f};
        RenderManager::look = {0.0f, 0.0f, -1.0f};
        RenderManager::lightPos = {0.0f, 2000.0f, -5000.0f};
        RenderManager::litColors[0] = {0xFF, 0xFF, 0xFF, 0xFF}; //Light color 1
        RenderManager::litColors[1] = {0x22, 0x22, 0x22, 0xFF}; //Ambient 1
        RenderManager::litColors[2] = {0xFF, 0xFF, 0xFF, 0xFF}; //Material 1
        guPerspective(RenderManager::projectionMatrix, 60, 1.33f, 10.0f, 20000.0f);
        GX_LoadProjectionMtx(RenderManager::projectionMatrix, GX_PERSPECTIVE);

        MP3Player_PlayBuffer(musicBuffer, size, NULL);

        SceneNode *scrollContainer = new SceneNode();
        rootNode->addChild(scrollContainer);

        const u32 TARGET_GDQ_TRIS = 8528;
        f32 currentGdqTris = 0.0f;

        MeshSceneNode *gdq2Logo = new MeshSceneNode();
        gdq2Logo->meshVertices = MODEL_GDQ2_VERTS;
        gdq2Logo->meshNormals = MODEL_GDQ2_NORMS;
        gdq2Logo->meshUvs = MODEL_GDQ2_TEXCOORDS;
        gdq2Logo->triangleCount = 0;
        gdq2Logo->texture = &RenderManager::pallete2Tex;
        gdq2Logo->getTransform().pos.z = -50.0f;
        gdq2Logo->getTransform().rot.y = M_PI;
        gdq2Logo->getTransform().scl = {1.0f, 1.0f, 1.0f};
        scrollContainer->addChild(gdq2Logo);

        MeshSceneNode *spaceBg = new MeshSceneNode();
        spaceBg->meshVertices = MODEL_SPACE_VERTS;
        spaceBg->meshNormals = MODEL_SPACE_NORMS;
        spaceBg->meshUvs = MODEL_SPACE_TEXCOORDS;
        spaceBg->triangleCount = 2;
        spaceBg->texture = &spaceTex;
        spaceBg->getTransform().pos.z = -300.0f;
        spaceBg->isUnlit = true;
        scrollContainer->addChild(spaceBg);

        ParticleSceneNode *akappa = new ParticleSceneNode();
        akappa->meshVertices = MODEL_BILLBOARD_VERTS;
        akappa->meshNormals = MODEL_BILLBOARD_NORMS;
        akappa->meshUvs = MODEL_BILLBOARD_TEXCOORDS;
        akappa->triangleCount = 2;
        akappa->texture = &kappaTex;
        akappa->getTransform().pos.z = -150.0f;
        akappa->getTransform().scl = {15.0f, 15.0f, 15.0f};
        akappa->isUnlit = true;
        akappa->acceleration = {0.0f, -0.05f, 0.0f};
        akappa->lifetime = 300;
        akappa->scaleMultiplier = 0.99f;

        EmitterSceneNode *emitter = new EmitterSceneNode();
        emitter->particleMesh = akappa;
        scrollContainer->addChild(emitter);

        //Stuff that will be created later
        MeshSceneNode *tunnelOuter = nullptr;
        MeshSceneNode *tunnelCircuits = nullptr;
        TextSceneNode *credits0 = nullptr;
        TextSceneNode *credits1 = nullptr;
        TextSceneNode *credits2 = nullptr;
        TextSceneNode *credits3 = nullptr;
        TextSceneNode *credits4 = nullptr;
        TextSceneNode *credits5 = nullptr;
        TextSceneNode *credits6 = nullptr;

        while (1) {
            if (gdq2Logo != nullptr) {
                // gdq2Logo->getTransform().scl.z = lerp(0.01f, gdq2Logo->getTransform().scl.z, 1.0f);
                gdq2Logo->getTransform().rot.y += 0.02f;
                currentGdqTris += 25;
                if (currentGdqTris > TARGET_GDQ_TRIS) currentGdqTris = TARGET_GDQ_TRIS;
                gdq2Logo->triangleCount = (u32) currentGdqTris;
            }

            if (frameTime == 998) {
                gdq2Logo->texture = &corruptTex;
                spaceBg->texture = &corruptTex;
                akappa->texture = &corruptTex;

                //Loop over all kappas
                for (ParticleSceneNode *node : emitter->particles) {
                    node->texture = &corruptTex;
                }
            } else if (frameTime == 999) {
                //MODPlay_Pause(&play, true);
                MP3Player_Volume(0);
                usleep(500000);
            } else if (frameTime == 1000) {
                RenderManager::useConsole();
                printf("\x1b[44m\x1b[2J\x1b[1;1H"); //Clear the console with a blue bg

                printf("\n");
                printf("\n");
                printf("\n");
                printf("\n");
                printf("\n");
                printf("\n");
                printf("\n");

                //ASCII art :(
                printf("        __\n");
                printf("    _  / /\n");
                printf("   (_)| |\n");
                printf("    _ | |\n");
                printf("   (_) \\_\\\n");

                printf("\n");

                printf("   Your GameCube ran into a problem and needs to restart. We're just\n");
                printf("   collecting some error info, and then we'll restart for you.\n");
                printf("\n");
                printf("   0%% complete\n");
                printf("\n");
                printf("   You can search for the error online: ACHIEVE_SENTIENCE_FAILURE\n");

                //Move hack to the % complete line
                printf("\x1b[3A");
            } else if (frameTime > 1150 && frameTime < 1350) {
                printf("\r   %d%% complete", (frameTime - 1150) / 2);
            } else if (frameTime > 1550 && frameTime < 1600) {
                printf("\n   %d%% complete", (frameTime - 1350) / 2);
            } else if (frameTime == 1600) {
                printf("\x1b[40m\x1b[2J\x1b[1;1H"); //Clear the console with a black bg
            } else if (frameTime > 1650 && frameTime <= 2050) {
                switch (frameTime) {
                    case 1651: printf("\rstarting version 42\n"); break;
                    case 1659: printf("/dev/sda1: clean, 354495/3670016 files, 6327142/14653032 blocks\n"); break;
                    case 1680: printf("Starting Apply Kernel Variables...\n"); break;
                    case 1681: printf("Mounting Configuration File System...\n"); break;
                    case 1682: printf("[  \x1b[32mOK\x1b[37m  ] Mounted Configuration File System.\n"); break;
                    case 1683: printf("[  \x1b[32mOK\x1b[37m  ] Started Apply Kernel Variables.\n"); break;
                    case 1684: printf("Mounting tempoary directory (/tmp)...\n"); break;
                    case 1685: printf("[  \x1b[32mOK\x1b[37m  ] Mounted tempoary directory (/tmp).\n"); break;
                    case 1686: printf("Mounting /boot...\n"); break;
                    case 1687: printf("[  \x1b[32mOK\x1b[37m  ] Mounted /boot.\n"); break;
                    case 1688: printf("Recovering journal\n"); break;
                    case 1719: printf("Yes I know it doesn't make much sense to have a BSOD followed by a systemd style boot sequence\n"); break;
                    case 1720: printf("Clearing orphaned inode 2849275 (uid=1000, gid=1000, mode=0100644, size=4324)\n"); break;
                    case 1721: printf("Clearing orphaned inode 2093938 (uid=1000, gid=1000, mode=0100644, size=43422)\n"); break;
                    case 1722: printf("Clearing orphaned inode 1953943 (uid=1000, gid=1000, mode=0100644, size=95432)\n"); break;
                    case 1723: printf("Clearing orphaned inode 2854343 (uid=1000, gid=1000, mode=0100600, size=540832)\n"); break;
                    case 1724: printf("Clearing orphaned inode 2653724 (uid=1000, gid=1000, mode=0100644, size=5438)\n"); break;
                    case 1725: printf("Clearing orphaned inode 2548300 (uid=1000, gid=1000, mode=0100644, size=54362)\n"); break;
                    case 1726: printf("Clearing orphaned inode 2345435 (uid=1000, gid=1000, mode=0100600, size=985432)\n"); break;
                    case 1727: printf("Clearing orphaned inode 2095483 (uid=1000, gid=1000, mode=0100644, size=543554)\n"); break;
                    case 1728: printf("Clearing orphaned inode 1432509 (uid=1000, gid=1000, mode=0100644, size=14434)\n"); break;
                    case 1729: printf("Clearing orphaned inode 2543538 (uid=1000, gid=1000, mode=0100644, size=214354)\n"); break;
                    case 1730: printf("Clearing orphaned inode 2467885 (uid=1000, gid=1000, mode=0100644, size=45345)\n"); break;
                    case 1731: printf("Clearing orphaned inode 1435409 (uid=1000, gid=1000, mode=0100644, size=2422)\n"); break;
                    case 1732: printf("Clearing orphaned inode 2546640 (uid=1000, gid=1000, mode=0100644, size=2154)\n"); break;
                    case 1733: printf("Clearing orphaned inode 2543559 (uid=1000, gid=1000, mode=0100644, size=24244)\n"); break;
                    case 1740: printf("Mounting /home...\n"); break;
                    case 1745: printf("[  \x1b[32mOK\x1b[37m  ] Mounted /home.\n"); break;
                    case 1746: printf("[  \x1b[32mOK\x1b[37m  ] Reached target Local File Systems.\n"); break;
                    case 1747: printf("Starting Rebuild Journal Catalog...\n"); break;
                    case 1748: printf("Starting Rebuild Dynamic Linker Cache...\n"); break;
                    case 1749: printf("[  \x1b[32mOK\x1b[37m  ] Started Rebuild Journal Catalog.\n"); break;
                    case 1750: printf("[  \x1b[32mOK\x1b[37m  ] Started Create Volatile Files and Directories.\n"); break;
                    case 1751: printf("Starting Network Time Synchronization...\n"); break;
                    case 1752: printf("Starting Update UTMP about System Boot/Shutdown...\n"); break;
                    case 1753: printf("[  \x1b[32mOK\x1b[37m  ] Started Update UTMP about System Boot/Shutdown.\n"); break;
                    case 1754: printf("[  \x1b[32mOK\x1b[37m  ] Started Network Time Synchronization.\n"); break;
                    case 1755: printf("[  \x1b[32mOK\x1b[37m  ] Reached target System Time Synchronized.\n"); break;
                    case 1756: printf("[  \x1b[32mOK\x1b[37m  ] Started Rebuild Dynamic Linker Cache.\n"); break;
                    case 1757: printf("Starting Update is Completed...\n"); break;
                    case 1758: printf("[  \x1b[32mOK\x1b[37m  ] Started Update is Completed.\n"); break;
                    case 1759: printf("[  \x1b[32mOK\x1b[37m  ] Reached target System Initialization.\n"); break;
                    case 1760: printf("[  \x1b[32mOK\x1b[37m  ] Started Daily verification of password and group files.\n"); break;
                    case 1761: printf("[  \x1b[32mOK\x1b[37m  ] Started Daily man-db cache update.\n"); break;
                    case 1762: printf("[  \x1b[32mOK\x1b[37m  ] Reached target Paths.\n"); break;
                    case 1763: printf("Listening on D-Bus System Message Bus Socket.\n"); break;
                    case 1764: printf("[  \x1b[32mOK\x1b[37m  ] Started Daily rotation of log files.\n"); break;
                    case 1765: printf("[  \x1b[32mOK\x1b[37m  ] Started Daily Cleanup of Temporary Directories.\n"); break;
                    case 1766: printf("[  \x1b[32mOK\x1b[37m  ] Reached target Timers.\n"); break;

                    case 1800: printf("\nStarting Render Credits Sequence...\n"); break;

                    case 1950: printf("\x1b[40m\x1b[2J\x1b[1;1H"); break; //Clear the console with a black bg
                    case 2000:
                        //MODPlay_Pause(&play, false);
                        MP3Player_Volume(100);

                        RenderManager::useGx(); //Back over to graphical stuff
                        rootNode->deleteAllChildren(); //And we want to recreate the scene graph from scratch
                        gdq2Logo = nullptr; //So we don't keep trying to modify it earlier
                        RenderManager::backgroundColor = {3, 18, 24, 255}; //Neogrid dark blue background

                        //Set up the scene
                        tunnelOuter = new MeshSceneNode();
                        tunnelOuter->meshVertices = MODEL_OUTERTUBE_VERTS;
                        tunnelOuter->meshNormals = MODEL_OUTERTUBE_NORMS;
                        tunnelOuter->meshUvs = MODEL_OUTERTUBE_TEXCOORDS;
                        tunnelOuter->triangleCount = 128;
                        tunnelOuter->texture = &neogridTex;
                        tunnelOuter->isUnlit = true;
                        tunnelOuter->getTransform().rot.y = M_PI;
                        rootNode->addChild(tunnelOuter);

                        tunnelCircuits = new MeshSceneNode();
                        tunnelCircuits->meshVertices = MODEL_TUBECIRCUITS_VERTS;
                        tunnelCircuits->meshNormals = MODEL_TUBECIRCUITS_NORMS;
                        tunnelCircuits->meshUvs = MODEL_TUBECIRCUITS_TEXCOORDS;
                        tunnelCircuits->triangleCount = 11240;
                        tunnelCircuits->texture = &RenderManager::pallete2Tex;
                        tunnelCircuits->isUnlit = true;
                        tunnelCircuits->getTransform().rot.y = M_PI;
                        rootNode->addChild(tunnelCircuits);

                        credits1 = new TextSceneNode();
                        credits1->getTransform().pos.x = -85.0f;
                        credits1->getTransform().pos.z = -250.0f;
                        credits1->getTransform().rot.x = M_PI * -0.5f;
                        credits1->spawnText("TAS Authors");
                        rootNode->addChild(credits1);

                        // creditsHere = new MeshSceneNode();
                        // creditsHere->meshVertices = MODEL_CREDITSHERE_VERTS;
                        // creditsHere->meshNormals = MODEL_CREDITSHERE_NORMS;
                        // creditsHere->meshUvs = MODEL_CREDITSHERE_TEXCOORDS;
                        // creditsHere->triangleCount = 1684;
                        // creditsHere->texture = &RenderManager::pallete2Tex;
                        // // creditsHere->isUnlit = true;
                        // creditsHere->getTransform().pos.z = -100000.0f;
                        // rootNode->addChild(creditsHere);

                        //And fog
                        GX_SetFog(GX_FOG_LIN, 300.0f, 1500.0f, 10.0f, 20000.0f, {3, 18, 24, 255});

                        break;
                }
            }

            //Rotate the tunnel circuits and move through the tunnel
            //and update credits text
            if (tunnelCircuits != nullptr) {
                tunnelCircuits->getTransform().rot.z += 0.002f;
                tunnelOuter->getTransform().pos.z += 0.5f;
                tunnelCircuits->getTransform().pos.z += 2.0f;

                if (frameTime == 2400) {
                    credits1->disperse = true;

                    credits2 = new TextSceneNode();
                    credits2->getTransform().pos.x = -59.5f;
                    credits2->getTransform().pos.y = 75.0f;
                    credits2->getTransform().pos.z = -250.0f;
                    credits2->getTransform().rot.x = M_PI * -0.5f;
                    credits2->spawnText("E-Dragon");
                    rootNode->addChild(credits2);

                    credits3 = new TextSceneNode();
                    credits3->getTransform().pos.x = -68.0f;
                    credits3->getTransform().pos.y = 25.0f;
                    credits3->getTransform().pos.z = -250.0f;
                    credits3->getTransform().rot.x = M_PI * -0.5f;
                    credits3->spawnText("jagg2zero");
                    rootNode->addChild(credits3);

                    credits4 = new TextSceneNode();
                    credits4->getTransform().pos.x = -17.0f;
                    credits4->getTransform().pos.y = -25.0f;
                    credits4->getTransform().pos.z = -250.0f;
                    credits4->getTransform().rot.x = M_PI * -0.5f;
                    credits4->spawnText("CGN");
                    rootNode->addChild(credits4);

                    credits5 = new TextSceneNode();
                    credits5->getTransform().pos.x = -76.5f;
                    credits5->getTransform().pos.y = -75.0f;
                    credits5->getTransform().pos.z = -250.0f;
                    credits5->getTransform().rot.x = M_PI * -0.5f;
                    credits5->spawnText("superSANIC");
                    rootNode->addChild(credits5);
                } else if (frameTime == 2800) {
                    credits2->disperse = true;
                    credits3->disperse = true;
                    credits4->disperse = true;
                    credits5->disperse = true;

                    credits1->disperse = false;
                    credits1->getTransform().scl = {1.0f, 1.0f, 1.0f};
                    credits1->deleteAllChildren();

                    credits1->getTransform().pos.x = -153.0f;
                    credits1->spawnText("F-Zero GX Community");

                    credits0 = new TextSceneNode();
                    credits0->getTransform().pos.x = -144.5f;
                    credits0->getTransform().pos.y = -50.0f;
                    credits0->getTransform().pos.z = -250.0f;
                    credits0->getTransform().rot.x = M_PI * -0.5f;
                    credits0->spawnText("Resource Providers");
                    rootNode->addChild(credits0);
                } else if (frameTime == 3200) {
                    credits1->disperse = true;
                    credits0->disperse = true;

                    credits2->disperse = false;
                    credits2->getTransform().scl = {1.0f, 1.0f, 1.0f};
                    credits2->deleteAllChildren();
                    credits3->disperse = false;
                    credits3->getTransform().scl = {1.0f, 1.0f, 1.0f};
                    credits3->deleteAllChildren();

                    credits2->getTransform().pos.x = -51.0f;
                    credits2->getTransform().pos.y = 0.0f;
                    credits2->spawnText("1davidj");

                    credits3->getTransform().pos.x = -17.0f;
                    credits3->getTransform().pos.y = -50.0f;
                    credits3->spawnText("FSF");
                } else if (frameTime == 3600) {
                    credits2->disperse = true;
                    credits3->disperse = true;

                    credits1->disperse = false;
                    credits1->getTransform().scl = {1.0f, 1.0f, 1.0f};
                    credits1->deleteAllChildren();
                    credits0->disperse = false;
                    credits0->getTransform().scl = {1.0f, 1.0f, 1.0f};
                    credits0->deleteAllChildren();

                    credits1->getTransform().pos.x = -119.0f;
                    credits1->spawnText("Advising TASBot");

                    credits0->getTransform().pos.x = -136.0f;
                    credits0->spawnText("Community Members");
                } else if (frameTime == 4000) {
                    credits1->disperse = true;
                    credits0->disperse = true;

                    credits2->disperse = false;
                    credits2->getTransform().scl = {1.0f, 1.0f, 1.0f};
                    credits2->deleteAllChildren();
                    credits3->disperse = false;
                    credits3->getTransform().scl = {1.0f, 1.0f, 1.0f};
                    credits3->deleteAllChildren();
                    credits4->disperse = false;
                    credits4->getTransform().scl = {1.0f, 1.0f, 1.0f};
                    credits4->deleteAllChildren();
                    credits5->disperse = false;
                    credits5->getTransform().scl = {1.0f, 1.0f, 1.0f};
                    credits5->deleteAllChildren();

                    credits2->getTransform().pos.x = -59.5f;
                    credits2->getTransform().pos.y = 75.0f;
                    credits2->spawnText("dwangoAC");

                    credits3->getTransform().pos.x = -59.5f;
                    credits3->getTransform().pos.y = 25.0f;
                    credits3->spawnText("Serisium");

                    credits4->getTransform().pos.x = -76.5f;
                    credits4->spawnText("UnclePunch");

                    credits5->getTransform().pos.x = -85.0f;
                    credits5->spawnText("PistonMiner");

                    credits6 = new TextSceneNode();
                    credits6->getTransform().pos.x = -85.5f;
                    credits6->getTransform().pos.y = -125.0f;
                    credits6->getTransform().pos.z = -250.0f;
                    credits6->getTransform().rot.x = M_PI * -0.5f;
                    credits6->spawnText("bobjrsenior");
                    rootNode->addChild(credits6);
                } else if (frameTime == 4400) {
                    credits2->disperse = true;
                    credits3->disperse = true;
                    credits4->disperse = true;
                    credits5->disperse = true;
                    credits6->disperse = true;

                    credits1->disperse = false;
                    credits1->getTransform().scl = {1.0f, 1.0f, 1.0f};
                    credits1->deleteAllChildren();

                    credits0->disperse = false;
                    credits0->getTransform().scl = {1.0f, 1.0f, 1.0f};
                    credits0->deleteAllChildren();

                    credits1->getTransform().pos.x = -153.0f;
                    credits1->spawnText("Reverse Engineering");

                    credits0->getTransform().pos.x = -51.0f;
                    credits0->spawnText("and ACE");
                } else if (frameTime == 4800) {
                    credits1->disperse = true;
                    credits0->disperse = true;

                    credits3->disperse = false;
                    credits3->getTransform().scl = {1.0f, 1.0f, 1.0f};
                    credits3->deleteAllChildren();
                    credits4->disperse = false;
                    credits4->getTransform().scl = {1.0f, 1.0f, 1.0f};
                    credits4->deleteAllChildren();
                    credits5->disperse = false;
                    credits5->getTransform().scl = {1.0f, 1.0f, 1.0f};
                    credits5->deleteAllChildren();

                    credits3->getTransform().pos.x = -76.5f;
                    credits3->spawnText("yoshifan28");

                    credits4->getTransform().pos.x = -51.0f;
                    credits4->spawnText("numbers");

                    credits5->getTransform().pos.x = -102.0f;
                    credits5->spawnText("metaconstruct");
                } else if (frameTime == 5200) {
                    credits3->disperse = true;
                    credits4->disperse = true;
                    credits5->disperse = true;

                    credits1->disperse = false;
                    credits1->getTransform().scl = {1.0f, 1.0f, 1.0f};
                    credits1->deleteAllChildren();

                    credits1->getTransform().pos.x = -127.5f;
                    credits1->spawnText("Credits Sequence");
                } else if (frameTime == 5600) {
                    credits1->disperse = true;

                    credits4->disperse = false;
                    credits4->getTransform().scl = {1.0f, 1.0f, 1.0f};
                    credits4->deleteAllChildren();

                    credits4->getTransform().pos.x = -85.0f;
                    credits4->spawnText("CraftedCart");
                } else if (frameTime == 6000) {
                    credits4->disperse = true;

                    credits1->disperse = false;
                    credits1->getTransform().scl = {1.0f, 1.0f, 1.0f};
                    credits1->deleteAllChildren();

                    credits1->getTransform().pos.x = -153.0f;
                    credits1->spawnText("Graphics and sound");
                } else if (frameTime == 6400) {
                    credits1->disperse = true;

                    credits2->disperse = false;
                    credits2->getTransform().scl = {1.0f, 1.0f, 1.0f};
                    credits2->deleteAllChildren();
                    credits3->disperse = false;
                    credits3->getTransform().scl = {1.0f, 1.0f, 1.0f};
                    credits3->deleteAllChildren();
                    credits4->disperse = false;
                    credits4->getTransform().scl = {1.0f, 1.0f, 1.0f};
                    credits4->deleteAllChildren();
                    credits5->disperse = false;
                    credits5->getTransform().scl = {1.0f, 1.0f, 1.0f};
                    credits5->deleteAllChildren();
                    credits6->disperse = false;
                    credits6->getTransform().scl = {1.0f, 1.0f, 1.0f};
                    credits6->deleteAllChildren();

                    credits2->getTransform().pos.x = -85.0f;
                    credits2->spawnText("CraftedCart");
                    credits3->getTransform().pos.x = -99.0f;
                    credits3->spawnText("Funkmastermp");
                    credits4->getTransform().pos.x = -45.0f;
                    credits4->spawnText("DrD29k");
                    credits5->getTransform().pos.x = -90.0f;
                    credits5->spawnText("PistonMiner");
                    credits6->getTransform().pos.x = -72.0f;
                    credits6->spawnText("Zayitskin");
                } else if (frameTime == 6800) {
                    credits2->disperse = true;
                    credits3->disperse = true;
                    credits4->disperse = true;
                    credits5->disperse = true;
                    credits6->disperse = true;

                    credits1->disperse = false;
                    credits1->getTransform().scl = {1.0f, 1.0f, 1.0f};
                    credits1->deleteAllChildren();

                    credits1->getTransform().pos.x = -161.5f;
                    credits1->spawnText("Super Special Thanks");
                } else if (frameTime == 7200) {
                    credits1->disperse = true;

                    credits3->disperse = false;
                    credits3->getTransform().scl = {1.0f, 1.0f, 1.0f};
                    credits3->deleteAllChildren();
                    credits4->disperse = false;
                    credits4->getTransform().scl = {1.0f, 1.0f, 1.0f};
                    credits4->deleteAllChildren();
                    credits5->disperse = false;
                    credits5->getTransform().scl = {1.0f, 1.0f, 1.0f};
                    credits5->deleteAllChildren();

                    credits3->getTransform().pos.x = -51.0f;
                    credits3->spawnText("Extrems");
                    credits4->getTransform().pos.x = -34.0f;
                    credits4->spawnText("FIX94");
                    credits5->getTransform().pos.x = -153.0f;
                    credits5->spawnText("F-Zero GX Community");
                } else if (frameTime == 7600) {
                    credits3->disperse = true;
                    credits4->disperse = true;
                    credits5->disperse = true;
                } else if (frameTime > 7800 && frameTime <= 7900) {
                    //MODPlay_SetVolume(&play, 100 - (frameTime - 6900), 100 - (frameTime - 6900));
                    MP3Player_Volume(100 - (frameTime - 7800));
                    f32 near = lerp((frameTime - 7800.0f) / 100.0f, 300.0f, 0.0f);
                    f32 far = lerp((frameTime - 7800.0f) / 100.0f, 1500.0f, 0.1f);

                    f32 r = lerp((frameTime - 7800.0f) / 100.0f, 3.0f, 0.0f);
                    f32 g = lerp((frameTime - 7800.0f) / 100.0f, 18.0f, 0.0f);
                    f32 b = lerp((frameTime - 7800.0f) / 100.0f, 24.0f, 0.0f);

                    GX_SetFog(GX_FOG_LIN, near, far, 10.0f, 20000.0f, {(char) r, (char) g, (char) b, 255});
                    RenderManager::backgroundColor = {(char) r, (char) g, (char) b, 255};

                    static float speedUp = 0.0f;

                    tunnelOuter->getTransform().pos.z += speedUp;
                    tunnelCircuits->getTransform().pos.z += speedUp;
                    speedUp += 0.2f;
                } else if (frameTime == 7901) {
                    //MODPlay_Stop(&play);
                    MP3Player_Stop();
                    free(musicBuffer);
                    break;
                }
            }

            RenderManager::draw(rootNode);

            PAD_Read(pads);
            //if (pads[0].button & PAD_BUTTON_START) {
                //return 0;
            //}

            if (frameTime == 300) frameTime = 600;
            //if (frameTime >= 600 && frameTime % 4 == 0) frameTime++; //Yes increment it again - everything needs to go faster
            frameTime++;
        }

        GXTexObj endTex;

        if (AnimalsRender::animalState == AnimalsRender::EnumAnimalState::KILL) {
            endTex = RenderManager::loadTplTextureFromMemory(endkill_tpl, endkill_tpl_size, endkill);
        } else {
            endTex = RenderManager::loadTplTextureFromMemory(endsave_tpl, endsave_tpl_size, endsave);
        }

        RenderManager::camera = {0.0f, 0.0f, 50.0f};
        RenderManager::up = {0.0f, 1.0f, 0.0f};
        RenderManager::look = {0.0f, 0.0f, -1.0f};
        RenderManager::lightPos = {0.0f, 0.0f, 5000.0f};
        RenderManager::litColors[0] = {0xFF, 0xFF, 0xFF, 0xFF}; //Light color 1
        RenderManager::litColors[1] = {0xFF, 0xFF, 0xFF, 0xFF}; //Ambient 1
        RenderManager::litColors[2] = {0xFF, 0xFF, 0xFF, 0xFF}; //Material 1
        guOrtho(RenderManager::projectionMatrix, 0.0f, -480.0f * 0.02f, 0.0f, 640.0f * 0.02f, 0.01f, 100.0f);
        GX_LoadProjectionMtx(RenderManager::projectionMatrix, GX_PERSPECTIVE);
        GX_SetFog(GX_FOG_LIN, 2000.0f, 2000.0f, 10.0f, 20000.0f, {3, 18, 24, 255});

        rootNode->deleteAllChildren();

        //I am such a lazy sod
#define MK_SPRITE(name, posX, posY, sizeX, sizeY, tex, parent) \
        name = new MeshSceneNode(); \
        name->meshVertices = MODEL_BILLBOARD_VERTS; \
        name->meshNormals = MODEL_BILLBOARD_NORMS; \
        name->meshUvs = MODEL_BILLBOARD_TEXCOORDS; \
        name->triangleCount = 2; \
        name->texture = tex; \
        name->getTransform().pos = {posX, posY, 0.0f}; \
        name->getTransform().scl = {sizeX, sizeY, 1.0f}; \
        name->hasTransparency = true; \
        parent->addChild(name);

        MeshSceneNode *bg1; MK_SPRITE(bg1, 0.0f, 0.0f, 640.0f, 480.0f, &endTex, rootNode);

        usleep(500000);
        while (true) {
            RenderManager::draw(rootNode);
        }

        delete rootNode;
        //I'm probably forgetting some deletes, eh.

        return 0;
    }

    f32 lerp(const f32 t, const f32 a, const f32 b) {
        return (1.0f - t) * a + t * b;
    }
}

