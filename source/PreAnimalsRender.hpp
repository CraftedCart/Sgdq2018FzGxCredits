#ifndef GDQ_RENDER_PREANIMALSRENDER_HPP
#define GDQ_RENDER_PREANIMALSRENDER_HPP

#include <gccore.h>

/**
 * @brief color-a-dinosaur
 */
namespace PreAnimalsRender {
    int execute();
    void slowType(const char *str, u32 delay);
}

#endif

