#include "SmokeSceneNode.hpp"

void SmokeSceneNode::tick() {
    if (getTransform().scl.x > 0.0f) {
        getTransform().scl.x += scaleMultiplier;
        getTransform().scl.y += scaleMultiplier;
        getTransform().scl.z += scaleMultiplier;

        scaleMultiplier -= 0.005f;
    } else {
        getTransform().scl = {0.0f, 0.0f, 0.0f};
    }
}

