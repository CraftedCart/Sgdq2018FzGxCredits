#ifndef GDQ_RENDER_GDQRENDER_HPP
#define GDQ_RENDER_GDQRENDER_HPP

#include "SceneNode.hpp"
#include <gccore.h>

namespace GdqRender {
    extern SceneNode *rootNode;
    extern PADStatus pads[4];

    int execute();

    f32 lerp(const f32 t, const f32 a, const f32 b);
}

#endif

