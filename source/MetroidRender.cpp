#include "PreAnimalsRender.hpp"
#include "RenderManager.hpp"
#include "AnimalSceneNode.hpp"
#include "CardManager.hpp"
#include "metroidbg_tpl.h"
#include "metroidbg.h"
#include "metroidbg2_tpl.h"
#include "metroidbg2.h"
#include "metroidbg3_tpl.h"
#include "metroidbg3.h"
#include "metroidbg4_tpl.h"
#include "metroidbg4.h"
#include "metroidbg5_tpl.h"
#include "metroidbg5.h"
#include "metroidbg6_tpl.h"
#include "metroidbg6.h"
#include "samusship_tpl.h"
#include "samusship.h"
#include "ring_tpl.h"
#include "ring.h"
#include <mp3player.h>
#include <string>
#include <cstdio>
#include <cstring>
#include <unistd.h>

//Super hacky but I'm a lazy sod
namespace MetroidModels {
#include "models/billboardverts.h"
#include "models/billboardnorms.h"
#include "models/billboardtexcoords.h"
}

#define MK_SPRITE(name, posX, posY, sizeX, sizeY, tex, parent) \
        name = new MeshSceneNode(); \
        name->meshVertices = MetroidModels::MODEL_BILLBOARD_VERTS; \
        name->meshNormals = MetroidModels::MODEL_BILLBOARD_NORMS; \
        name->meshUvs = MetroidModels::MODEL_BILLBOARD_TEXCOORDS; \
        name->triangleCount = 2; \
        name->texture = tex; \
        name->getTransform().pos = {posX, posY, 0.0f}; \
        name->getTransform().scl = {sizeX, sizeY, 1.0f}; \
        name->hasTransparency = true; \
        parent->addChild(name);

#define MK_ANIMAL(name, posX, posY, sizeX, sizeY, tex, parent) \
        AnimalSceneNode *name = new AnimalSceneNode(); \
        name->meshVertices = MetroidModels::MODEL_BILLBOARD_VERTS; \
        name->meshNormals = MetroidModels::MODEL_BILLBOARD_NORMS; \
        name->meshUvs = MetroidModels::MODEL_BILLBOARD_TEXCOORDS; \
        name->triangleCount = 2; \
        name->texture = tex; \
        name->getTransform().pos = {posX, posY, 0.0f}; \
        name->getTransform().scl = {sizeX, sizeY, 1.0f}; \
        name->hasTransparency = true; \
        name->shouldJump = false; \
        name->gravity = 0.2f; \
        parent->addChild(name);

namespace MetroidRender {
    SceneNode *rootNode;

    int execute() {
        rootNode = new SceneNode();

        //int windSize;
        //void *windBuffer = CardManager::openFile("fzgxwind", &windSize);
        //MP3Player_PlayBuffer(windBuffer, windSize, NULL);

        GXTexObj bgTex = RenderManager::loadTplTextureFromMemory(metroidbg_tpl, metroidbg_tpl_size, metroidbg);
        GX_InitTexObjLOD(&bgTex, GX_NEAR, GX_NEAR, 0.0f, 1.0f, 0.0f, GX_DISABLE, GX_DISABLE, GX_ANISO_1);
        GXTexObj bgTex2 = RenderManager::loadTplTextureFromMemory(metroidbg2_tpl, metroidbg2_tpl_size, metroidbg2);
        GX_InitTexObjLOD(&bgTex2, GX_NEAR, GX_NEAR, 0.0f, 1.0f, 0.0f, GX_DISABLE, GX_DISABLE, GX_ANISO_1);
        GXTexObj bgTex3 = RenderManager::loadTplTextureFromMemory(metroidbg3_tpl, metroidbg3_tpl_size, metroidbg3);
        GX_InitTexObjLOD(&bgTex3, GX_NEAR, GX_NEAR, 0.0f, 1.0f, 0.0f, GX_DISABLE, GX_DISABLE, GX_ANISO_1);
        GXTexObj bgTex4 = RenderManager::loadTplTextureFromMemory(metroidbg4_tpl, metroidbg4_tpl_size, metroidbg4);
        GX_InitTexObjLOD(&bgTex4, GX_NEAR, GX_NEAR, 0.0f, 1.0f, 0.0f, GX_DISABLE, GX_DISABLE, GX_ANISO_1);
        GXTexObj bgTex5 = RenderManager::loadTplTextureFromMemory(metroidbg5_tpl, metroidbg5_tpl_size, metroidbg5);
        GX_InitTexObjLOD(&bgTex5, GX_NEAR, GX_NEAR, 0.0f, 1.0f, 0.0f, GX_DISABLE, GX_DISABLE, GX_ANISO_1);
        GXTexObj bgTex6 = RenderManager::loadTplTextureFromMemory(metroidbg6_tpl, metroidbg6_tpl_size, metroidbg6);
        GX_InitTexObjLOD(&bgTex6, GX_NEAR, GX_NEAR, 0.0f, 1.0f, 0.0f, GX_DISABLE, GX_DISABLE, GX_ANISO_1);
        GXTexObj shipTex = RenderManager::loadTplTextureFromMemory(samusship_tpl, samusship_tpl_size, samusship);
        GX_InitTexObjLOD(&shipTex, GX_NEAR, GX_NEAR, 0.0f, 1.0f, 0.0f, GX_DISABLE, GX_DISABLE, GX_ANISO_1);
        GXTexObj ringTex = RenderManager::loadTplTextureFromMemory(ring_tpl, ring_tpl_size, ring);
        GX_InitTexObjLOD(&ringTex, GX_NEAR, GX_NEAR, 0.0f, 1.0f, 0.0f, GX_DISABLE, GX_DISABLE, GX_ANISO_1);

        RenderManager::camera = {0.0f, 0.0f, 50.0f};
        RenderManager::up = {0.0f, 1.0f, 0.0f};
        RenderManager::look = {0.0f, 0.0f, -1.0f};
        RenderManager::lightPos = {0.0f, 0.0f, 5000.0f};
        RenderManager::litColors[0] = {0xFF, 0xFF, 0xFF, 0xFF}; //Light color 1
        RenderManager::litColors[1] = {0xFF, 0xFF, 0xFF, 0xFF}; //Ambient 1
        RenderManager::litColors[2] = {0xFF, 0xFF, 0xFF, 0xFF}; //Material 1

        guOrtho(RenderManager::projectionMatrix, 0.0f, -480.0f * 0.02f, 0.0f, 640.0f * 0.02f, 0.01f, 100.0f);
        GX_LoadProjectionMtx(RenderManager::projectionMatrix, GX_PERSPECTIVE);

        SceneNode *scrollContainer = new SceneNode();
        rootNode->addChild(scrollContainer);

        //Create the background planes
        MeshSceneNode *bg1; MK_SPRITE(bg1, 0.0f, 0.0f, 640.0f, 480.0f, &bgTex, scrollContainer);
        MeshSceneNode *bg2; MK_SPRITE(bg2, 640.0f, 0.0f, 640.0f, 480.0f, &bgTex2, scrollContainer);
        MeshSceneNode *bg3; MK_SPRITE(bg3, 640.0f * 2, 0.0f, 640.0f, 480.0f, &bgTex3, scrollContainer);
        MeshSceneNode *bg4; MK_SPRITE(bg4, 640.0f * 3, 0.0f, 640.0f, 480.0f, &bgTex4, scrollContainer);
        MeshSceneNode *bg5; MK_SPRITE(bg5, 640.0f * 4, 0.0f, 640.0f, 480.0f, &bgTex5, scrollContainer);
        MeshSceneNode *bg6; MK_SPRITE(bg6, 640.0f * 5, 0.0f, 640.0f, 480.0f, &bgTex6, scrollContainer);

        //Animals!
        MeshSceneNode *animalSpawn1; MK_SPRITE(animalSpawn1, -150.0f,  -28.0f, 80.0f, 80.0f, &RenderManager::animalTex1, rootNode);
        MeshSceneNode *animalSpawn2; MK_SPRITE(animalSpawn2, -100.0f, -28.0f, 80.0f, 80.0f, &RenderManager::animalTex2, rootNode);
        MeshSceneNode *animalSpawn3; MK_SPRITE(animalSpawn3, -20.0f, -60.0f, 80.0f, 80.0f, &RenderManager::animalTex3, rootNode);
        MeshSceneNode *animalSpawn4; MK_SPRITE(animalSpawn4, 50.0f, -90.0f, 80.0f, 80.0f, &RenderManager::animalTex4, rootNode);
        MeshSceneNode *animalSpawn5; MK_SPRITE(animalSpawn5, 150.0f, -115.0f, 80.0f, 80.0f, &RenderManager::animalTex5, rootNode);
        MeshSceneNode *animalSpawn6; MK_SPRITE(animalSpawn6, 200.0f, -70.0f, 160.0f, 160.0f, &RenderManager::animalTex6, rootNode);
        MeshSceneNode *animalSpawn7; MK_SPRITE(animalSpawn7, 300.0f, -115.0f, 80.0f, 80.0f, &RenderManager::animalTex7, rootNode);
        MeshSceneNode *animalSpawn8; MK_SPRITE(animalSpawn8, 350.0f, -70.0f, 160.0f, 160.0f, &RenderManager::animalTex8, rootNode);

        MeshSceneNode *ship; MK_SPRITE(ship, 1500.0f, -70.0f, 320.0f, 160.0f, &shipTex, rootNode);

        MK_ANIMAL(animalEnd1, 3100.0f, -56.0f, 80.0f, 80.0f, &RenderManager::animalTex1, rootNode);
        MK_ANIMAL(animalEnd2, 3130.0f, -56.0f, 80.0f, 80.0f, &RenderManager::animalTex2, rootNode);
        MK_ANIMAL(animalEnd3, 3160.0f, -56.0f, 80.0f, 80.0f, &RenderManager::animalTex3, rootNode);
        MK_ANIMAL(animalEnd4, 3190.0f, -56.0f, 80.0f, 80.0f, &RenderManager::animalTex4, rootNode);
        MK_ANIMAL(animalEnd6, 3350.0f, -15.0f, 160.0f, 160.0f, &RenderManager::animalTex6, rootNode);
        MK_ANIMAL(animalEnd7, 3400.0f, -56.0f, 80.0f, 80.0f, &RenderManager::animalTex7, rootNode);
        MK_ANIMAL(animalEnd8, 3450.0f, -15.0f, 160.0f, 160.0f, &RenderManager::animalTex8, rootNode);
        MK_ANIMAL(animalEnd5, 3220.0f, -56.0f, 80.0f, 80.0f, &RenderManager::animalTex5, rootNode);

        MeshSceneNode *portalGun; MK_SPRITE(portalGun, 3285.0f, -50.0f, 50.0f, 50.0f, &RenderManager::portalGunTex, rootNode);
        MeshSceneNode *pickupRing; MK_SPRITE(pickupRing, 3285.0f, -50.0f, 0.0f, 0.0f, &ringTex, rootNode);
        MeshSceneNode *portal;
        MeshSceneNode *portalPellet;

        u32 frameTime = 0;
        while (true) {
            RenderManager::draw(rootNode);

            static float scrollSpeed = -0.001f;
            static bool speedUp = true;
            scrollContainer->getTransform().pos.x += scrollSpeed;
            animalSpawn1->getTransform().pos.x += scrollSpeed * 640.0f;
            animalSpawn2->getTransform().pos.x += scrollSpeed * 640.0f;
            animalSpawn3->getTransform().pos.x += scrollSpeed * 640.0f;
            animalSpawn4->getTransform().pos.x += scrollSpeed * 640.0f;
            animalSpawn5->getTransform().pos.x += scrollSpeed * 640.0f;
            animalSpawn6->getTransform().pos.x += scrollSpeed * 640.0f;
            animalSpawn7->getTransform().pos.x += scrollSpeed * 640.0f;
            animalSpawn8->getTransform().pos.x += scrollSpeed * 640.0f;
            ship->getTransform().pos.x += scrollSpeed * 640.0f;
            animalEnd1->getTransform().pos.x += scrollSpeed * 640.0f;
            animalEnd2->getTransform().pos.x += scrollSpeed * 640.0f;
            animalEnd3->getTransform().pos.x += scrollSpeed * 640.0f;
            animalEnd4->getTransform().pos.x += scrollSpeed * 640.0f;
            animalEnd5->getTransform().pos.x += scrollSpeed * 640.0f;
            animalEnd6->getTransform().pos.x += scrollSpeed * 640.0f;
            animalEnd7->getTransform().pos.x += scrollSpeed * 640.0f;
            animalEnd8->getTransform().pos.x += scrollSpeed * 640.0f;
            portalGun->getTransform().pos.x += scrollSpeed * 640.0f;

            if (frameTime > 350) {
                ship->getTransform().pos.y += 1.0f;
                ship->getTransform().pos.x += 0.2f;
            }

            if (speedUp) {
                scrollSpeed -= 0.00002f;
            } else {
                scrollSpeed += 0.0002f;
            }

            if (scrollSpeed < -0.0135f) speedUp = false;
            if (scrollSpeed > 0.0f) scrollSpeed = 0.0f;

            if (frameTime == 700) {
                animalEnd5->shouldJump = true;
                animalEnd5->jumpDirection = {0.6f, 2.0f, 0.0f};
            } else if (frameTime > 800) {
                portalGun->getTransform().pos = animalEnd5->getTransform().pos;
                portalGun->getTransform().pos.y -= 18.0f;
            }

            if (frameTime > 820) {
                pickupRing->getTransform().pos = portalGun->getTransform().pos;
                static float ringScaleMod = 8.0f;
                pickupRing->getTransform().scl.x += ringScaleMod;
                pickupRing->getTransform().scl.y += ringScaleMod;
                ringScaleMod += 4.0f;
            }

            if (frameTime > 800 && frameTime < 820) {
                portalGun->getTransform().scl = {(821 - frameTime) * 20.0f, (821 - frameTime) * 20.0f, 1.0f};
            }


            if (frameTime == 870) {
                MK_SPRITE(portalPellet, 320.0f, -20.0f, 10.0f, 10.0f, &RenderManager::portalB1Tex, rootNode);
                portalPellet->getTransform().pos = portalGun->getTransform().pos;
            }

            if (frameTime > 870) {
                portalPellet->getTransform().pos.x += 8.0f;
            }

            if (frameTime == 900) {
                MK_SPRITE(portal, 320.0f, -20.0f, 150.0f, 150.0f, &RenderManager::portalB1Tex, rootNode);
            } else if (frameTime > 900) {
                int sprite = (frameTime / 10) % 3;
                switch (sprite) {
                    case 0:
                        portal->texture = &RenderManager::portalB1Tex; break;
                    case 1:
                        portal->texture = &RenderManager::portalB2Tex; break;
                    case 2:
                        portal->texture = &RenderManager::portalB3Tex; break;
                }
            }

            if (frameTime == 950) {
                animalEnd1->shouldJump = true;
                animalEnd2->shouldJump = true;
                animalEnd3->shouldJump = true;
                animalEnd4->shouldJump = true;
                animalEnd5->shouldJump = true;
                animalEnd6->shouldJump = true;
                animalEnd7->shouldJump = true;
                animalEnd8->shouldJump = true;
                animalEnd1->jumpDirection = {1.5f, 2.0f, 0.0f};
                animalEnd2->jumpDirection = {1.5f, 2.0f, 0.0f};
                animalEnd3->jumpDirection = {1.5f, 2.0f, 0.0f};
                animalEnd4->jumpDirection = {1.5f, 2.0f, 0.0f};
                animalEnd5->jumpDirection = {1.5f, 2.0f, 0.0f};
                animalEnd6->jumpDirection = {1.5f, 2.0f, 0.0f};
                animalEnd7->jumpDirection = {1.5f, 2.0f, 0.0f};
                animalEnd8->jumpDirection = {1.5f, 2.0f, 0.0f};
            } else if (frameTime == 1500) {
                break;
            }

            frameTime++;
        }

        //MP3Player_Stop();
        //free(windBuffer);

        delete rootNode;

        return 0;
    }
}

