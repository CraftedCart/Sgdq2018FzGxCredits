#include "CardManager.hpp"
#include "RenderManager.hpp"
#include <iostream>
#include <cstring>
#include <cstdlib>
#include <unistd.h>

namespace CardManager {
    unsigned char workArea[CARD_WORKAREA] ATTRIBUTE_ALIGN(32);
    std::vector<u8> vec;

    int init() {
        CARD_Init("GZFE", "8P");
        int mountRet = CARD_Mount(CARD_SLOTA, workArea, [](s32 chn, s32 result) {});

        if (mountRet != 0) {
            RenderManager::useConsole();
            std::cout << "Failed to mount card - Return code: " << mountRet << std::endl;
            usleep(500000);
            return 1;
        }

        return 0;
    }

    void* openFile(const char *fileName, int *outSize) {
        //RenderManager::useConsole();

        //Find our file
        card_dir dir;
        CARD_FindFirst(CARD_SLOTA, &dir, true);
        do {
            if (strcmp(reinterpret_cast<const char*>(dir.filename), fileName) == 0) {
                break;
            }
        } while(!CARD_FindNext(&dir));

        //std::cout << "Found file" << std::endl;

        card_file file;
        if (int openRet = CARD_OpenEntry(CARD_SLOTA, &dir, &file)) {
            RenderManager::useConsole();
            std::cout << "Failed to open file: " << fileName <<  " - Return code: " << openRet << std::endl;
            usleep(500000);
        }

        //std::cout << "Open file" << std::endl;

        //Fetch the file length stored in the GCI
        static unsigned char sizeBuffer[512] ATTRIBUTE_ALIGN(32);

        if (CARD_Read(&file, sizeBuffer, 512, 0x2000)) {
            RenderManager::useConsole();
            std::cout << "Failed to read file to fetch filesize: " << fileName << std::endl;
            usleep(500000);
        }

        //std::cout << "Read file 1" << std::endl;

        u32 fileSize = *reinterpret_cast<u32*>(sizeBuffer + 0x40);
        *outSize = fileSize;

        //Now that we know the size, read the file... in smaller segments
        u32 size = roundUp(fileSize, 0x2000);
        void *fileBuffer ATTRIBUTE_ALIGN(32) = aligned_alloc(32, size);

        for (u32 i = 0; i < size; i += 0x2000) {
            //std::cout << "Read file at offset " << i << " of " << size << std::endl;
            if (CARD_Read(&file, fileBuffer + i, 0x2000, 0x4000 + i)) {
                RenderManager::useConsole();
                std::cout << "Failed to read file: " << fileName << std::endl;
                usleep(500000);
            }
        }

        //std::cout << "Read file 2" << std::endl;
        //std::cout << "Data at 0x2200" << std::endl;
        //printf("%s\n", reinterpret_cast<const char*>(fileBuffer));
        //std::cout << "Data at 0x2400" << std::endl;
        //printf("%s\n", reinterpret_cast<const char*>(fileBuffer + 0x200));
        //std::cout << "Data at 0x2600" << std::endl;
        //printf("%s\n", reinterpret_cast<const char*>(fileBuffer + 0x400));
        //std::cout << "Data at 0x4000" << std::endl;
        //printf("%s\n", reinterpret_cast<const char*>(fileBuffer + 0x1800));
        //std::cout << "Data at 0x4200" << std::endl;
        //printf("%s\n", reinterpret_cast<const char*>(fileBuffer + 0x2000));
        //std::cout << "Data at 0x4400" << std::endl;
        //printf("%s\n", reinterpret_cast<const char*>(fileBuffer + 0x2200));

        return fileBuffer;
    }

    int roundUp(int numToRound, int multiple) {
        if (multiple == 0)
            return numToRound;

        int remainder = numToRound % multiple;
        if (remainder == 0)
            return numToRound;

        return numToRound + multiple - remainder;
    }
}

