#ifndef GDQ_RENDER_ANIMALSRENDER_HPP
#define GDQ_RENDER_ANIMALSRENDER_HPP

#include "SceneNode.hpp"
#include <gccore.h>

namespace AnimalsRender {
    enum EnumAnimalState {
        PURGATORY,
        KILL,
        SAVE
    };

    extern SceneNode *rootNode;
    extern EnumAnimalState animalState;

    int execute();
    guVector calcForwardsVector(guVector rot);
    guVector cross(guVector a, guVector b);
}

#endif

