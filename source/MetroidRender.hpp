#ifndef GDQ_RENDER_METROIDRENDER_HPP
#define GDQ_RENDER_METROIDRENDER_HPP

#include "SceneNode.hpp"
#include <gccore.h>

namespace MetroidRender {
    extern SceneNode *rootNode;

    int execute();
}

#endif

