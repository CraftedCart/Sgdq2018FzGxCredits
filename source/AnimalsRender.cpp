#include "AnimalsRender.hpp"
#include "GdqRender.hpp"
#include "CardManager.hpp"
#include "RenderManager.hpp"
#include "AnimalSceneNode.hpp"
#include "SmokeSceneNode.hpp"
#include "wallcircuits_tpl.h"
#include "wallcircuits.h"
#include "floortile_tpl.h"
#include "floortile.h"
#include "pallete3_tpl.h"
#include "pallete3.h"
#include "hazard_tpl.h"
#include "hazard.h"
#include "grass_tpl.h"
#include "grass.h"
#include "models/animals/roomwallsverts.h"
#include "models/animals/roomwallsnorms.h"
#include "models/animals/roomwallstexcoords.h"
#include "models/animals/roomfloorsverts.h"
#include "models/animals/roomfloorsnorms.h"
#include "models/animals/roomfloorstexcoords.h"
#include "models/animals/roomventsverts.h"
#include "models/animals/roomventsnorms.h"
#include "models/animals/roomventstexcoords.h"
#include "models/animals/roomdoorverts.h"
#include "models/animals/roomdoornorms.h"
#include "models/animals/roomdoortexcoords.h"
#include "models/animals/roomtrapdoorverts.h"
#include "models/animals/roomtrapdoornorms.h"
#include "models/animals/roomtrapdoortexcoords.h"
#include "models/animals/powerboxverts.h"
#include "models/animals/powerboxnorms.h"
#include "models/animals/powerboxtexcoords.h"
#include "models/animals/powerwiresverts.h"
#include "models/animals/powerwiresnorms.h"
#include "models/animals/powerwirestexcoords.h"
#include "models/animals/sheepverts.h"
#include "models/animals/sheepnorms.h"
#include "models/animals/sheeptexcoords.h"
#include "models/animals/turretsverts.h"
#include "models/animals/turretsnorms.h"
#include "models/animals/turretstexcoords.h"
#include "models/animals/turretslaserkillverts.h"
#include "models/animals/turretslaserkillnorms.h"
#include "models/animals/turretslaserkilltexcoords.h"
#include "models/animals/turretslasersaveverts.h"
#include "models/animals/turretslasersavenorms.h"
#include "models/animals/turretslasersavetexcoords.h"
#include "models/animals/gunfireverts.h"
#include "models/animals/gunfirenorms.h"
#include "models/animals/gunfiretexcoords.h"
#include "models/animals/smokeverts.h"
#include "models/animals/smokenorms.h"
#include "models/animals/smoketexcoords.h"
#include "models/animals/grassverts.h"
#include "models/animals/grassnorms.h"
#include "models/animals/grasstexcoords.h"
#include "models/animals/grassholeverts.h"
#include "models/animals/grassholenorms.h"
#include "models/animals/grassholetexcoords.h"

//Super hacky but I'm a lazy sod
namespace AnimalsModels {
#include "models/billboardverts.h"
#include "models/billboardnorms.h"
#include "models/billboardtexcoords.h"
}

#include "gunfireaudio_bin.h"
#include <gccore.h>
#include <asndlib.h>
#include <mp3player.h>
#include <cmath>
#include <unistd.h>

#define MK_ANIMAL(name, tex, posX, posZ, rotY) \
        AnimalSceneNode *name = new AnimalSceneNode(); \
        name->meshVertices = AnimalsModels::MODEL_BILLBOARD_VERTS; \
        name->meshNormals = AnimalsModels::MODEL_BILLBOARD_NORMS; \
        name->meshUvs = AnimalsModels::MODEL_BILLBOARD_TEXCOORDS; \
        name->triangleCount = 2; \
        name->texture = tex; \
        name->getTransform().rot.y = rotY; \
        name->getTransform().pos.y = -20.0f; \
        name->getTransform().pos.x = posX; \
        name->getTransform().pos.z = posZ; \
        name->getTransform().scl = {20.0f, 20.0f, 20.0f}; \
        name->hasTransparency = true; \
        rootNode->addChild(name);

namespace AnimalsRender {
    SceneNode *rootNode;
    EnumAnimalState animalState = EnumAnimalState::PURGATORY;

    int execute() {
        //MP3Player_Init();

        //int turretSize;
        //void *turretBuffer = CardManager::openFile("fzgxturret", &turretSize);
        int doorsSize;
        void *doorsBuffer = CardManager::openFile("fzgxdoors", &doorsSize);

        //Set up the scene graph
        rootNode = new SceneNode();

        // bool isBlue = true;
        // bool bDown = false;
        //Pause screen
        // while (1) {
        //     RenderManager::draw(rootNode);

        //     PAD_Read(pads);
        //     if (pads[0].button & PAD_BUTTON_A) {
        //         break;
        //     }

        //     if (pads[0].button & PAD_BUTTON_B && !bDown) {
        //         bDown = true;
        //         if (isBlue) {
        //             RenderManager::backgroundColor = {0, 255, 0, 255};
        //         } else {
        //             RenderManager::backgroundColor = {0, 0, 255, 255};
        //         }
        //         isBlue = !isBlue;
        //     }

        //     if (!(pads[0].button & PAD_BUTTON_B)) {
        //         bDown = false;
        //     }
        // }

        //Load in textures
        GXTexObj wallCircuitsTex = RenderManager::loadTplTextureFromMemory(wallcircuits_tpl, wallcircuits_tpl_size, wallcircuits);
        GXTexObj floorTileTex = RenderManager::loadTplTextureFromMemory(floortile_tpl, floortile_tpl_size, floortile);
        GXTexObj pallete3Tex = RenderManager::loadTplTextureFromMemory(pallete3_tpl, pallete3_tpl_size, pallete3);
        GXTexObj hazardTex = RenderManager::loadTplTextureFromMemory(hazard_tpl, hazard_tpl_size, hazard);
        GXTexObj grassTex = RenderManager::loadTplTextureFromMemory(grass_tpl, grass_tpl_size, grass);

        GX_SetBlendMode(GX_BM_BLEND, GX_BL_SRCALPHA, GX_BL_INVSRCALPHA, GX_LO_CLEAR);

        //Set lighting
        RenderManager::lightPos = {-5000.0f, 2000.0f, -5000.0f};
        RenderManager::litColors[0] = {0xFF, 0xFF, 0xFF, 0xFF}; //Light color 1
        RenderManager::litColors[1] = {0x52, 0x52, 0x52, 0xFF}; //Ambient 1
        RenderManager::litColors[2] = {0xFF, 0xFF, 0xFF, 0xFF}; //Material 1

        //Set camera
        RenderManager::camera = {-37.9688f, -26.7648, 27.0126f};
        //guVector rot = {1.8044f, 0.000007f, -0.933711f};
        //RenderManager::look = calcForwardsVector(rot);
        //guVector right = {
                          //-sin(rot.x - M_PI * 0.5),
                          //0.0,
                          //-cos(rot.x - M_PI * 0.5)
                          //};
        //RenderManager::up = cross(right, RenderManager::look);
        //FOV 90 and other stuff
        guPerspective(RenderManager::projectionMatrix, 80, 1.33f, 2.0f, 2000.0f);
        GX_LoadProjectionMtx(RenderManager::projectionMatrix, GX_PERSPECTIVE);

        //Sky color
        RenderManager::backgroundColor = {170, 200, 247, 255};

        //Unfoggify
        GX_SetFog(GX_FOG_LIN, 300.0f, 1500.0f, 2.0f, 2000.0f, {170, 200, 247, 255});

        MeshSceneNode *roomWall = new MeshSceneNode();
        roomWall->meshVertices = MODEL_ROOMWALLS_VERTS;
        roomWall->meshNormals = MODEL_ROOMWALLS_NORMS;
        roomWall->meshUvs = MODEL_ROOMWALLS_TEXCOORDS;
        roomWall->triangleCount = MODEL_ROOMWALLS_TRIS;
        roomWall->texture = &wallCircuitsTex;
        rootNode->addChild(roomWall);

        MeshSceneNode *roomFloor = new MeshSceneNode();
        roomFloor->meshVertices = MODEL_ROOMFLOORS_VERTS;
        roomFloor->meshNormals = MODEL_ROOMFLOORS_NORMS;
        roomFloor->meshUvs = MODEL_ROOMFLOORS_TEXCOORDS;
        roomFloor->triangleCount = MODEL_ROOMFLOORS_TRIS;
        roomFloor->texture = &floorTileTex;
        rootNode->addChild(roomFloor);

        MeshSceneNode *roomVent = new MeshSceneNode();
        roomVent->meshVertices = MODEL_ROOMVENTS_VERTS;
        roomVent->meshNormals = MODEL_ROOMVENTS_NORMS;
        roomVent->meshUvs = MODEL_ROOMVENTS_TEXCOORDS;
        roomVent->triangleCount = MODEL_ROOMVENTS_TRIS;
        roomVent->texture = &pallete3Tex;
        rootNode->addChild(roomVent);

        MeshSceneNode *roomDoor = new MeshSceneNode();
        roomDoor->meshVertices = MODEL_ROOMDOOR_VERTS;
        roomDoor->meshNormals = MODEL_ROOMDOOR_NORMS;
        roomDoor->meshUvs = MODEL_ROOMDOOR_TEXCOORDS;
        roomDoor->triangleCount = MODEL_ROOMDOOR_TRIS;
        roomDoor->texture = &floorTileTex;
        rootNode->addChild(roomDoor);

        MeshSceneNode *roomTrapdoor = new MeshSceneNode();
        roomTrapdoor->meshVertices = MODEL_ROOMTRAPDOOR_VERTS;
        roomTrapdoor->meshNormals = MODEL_ROOMTRAPDOOR_NORMS;
        roomTrapdoor->meshUvs = MODEL_ROOMTRAPDOOR_TEXCOORDS;
        roomTrapdoor->triangleCount = MODEL_ROOMTRAPDOOR_TRIS;
        roomTrapdoor->texture = &hazardTex;
        roomTrapdoor->getTransform().pos = {27.0f, -30.4f, 0.0f};
        rootNode->addChild(roomTrapdoor);

        MeshSceneNode *powerBox = new MeshSceneNode();
        powerBox->meshVertices = MODEL_POWERBOX_VERTS;
        powerBox->meshNormals = MODEL_POWERBOX_NORMS;
        powerBox->meshUvs = MODEL_POWERBOX_TEXCOORDS;
        powerBox->triangleCount = MODEL_POWERBOX_TRIS;
        powerBox->texture = &floorTileTex;
        powerBox->getTransform().rot.y = M_PI;
        rootNode->addChild(powerBox);

        MeshSceneNode *powerWires = new MeshSceneNode();
        powerWires->meshVertices = MODEL_POWERWIRES_VERTS;
        powerWires->meshNormals = MODEL_POWERWIRES_NORMS;
        powerWires->meshUvs = MODEL_POWERWIRES_TEXCOORDS;
        powerWires->triangleCount = MODEL_POWERWIRES_TRIS;
        powerWires->texture = &pallete3Tex;
        powerWires->getTransform().rot.y = M_PI;
        rootNode->addChild(powerWires);

        MeshSceneNode *grassPlane = new MeshSceneNode();
        grassPlane->meshVertices = MODEL_GRASS_VERTS;
        grassPlane->meshNormals = MODEL_GRASS_NORMS;
        grassPlane->meshUvs = MODEL_GRASS_TEXCOORDS;
        grassPlane->triangleCount = MODEL_GRASS_TRIS;
        grassPlane->texture = &grassTex;
        grassPlane->getTransform().rot.y = M_PI;
        rootNode->addChild(grassPlane);

        MeshSceneNode *hole = new MeshSceneNode();
        hole->meshVertices = MODEL_GRASSHOLE_VERTS;
        hole->meshNormals = MODEL_GRASSHOLE_NORMS;
        hole->meshUvs = MODEL_GRASSHOLE_TEXCOORDS;
        hole->triangleCount = MODEL_GRASSHOLE_TRIS;
        hole->texture = &pallete3Tex;
        rootNode->addChild(hole);

        MeshSceneNode *portal = new MeshSceneNode();
        portal->meshVertices = AnimalsModels::MODEL_BILLBOARD_VERTS;
        portal->meshNormals = AnimalsModels::MODEL_BILLBOARD_NORMS;
        portal->meshUvs = AnimalsModels::MODEL_BILLBOARD_TEXCOORDS;
        portal->triangleCount = 2;
        portal->texture = &RenderManager::portalO1Tex;
        portal->getTransform().pos.y = 25.0f;
        portal->getTransform().rot.x = M_PI * 0.5f;
        portal->getTransform().scl = {50.0f, 50.0f, 50.0f};
        rootNode->addChild(portal);

        MeshSceneNode *turrets[12];
        MeshSceneNode *turretLasers[12];

        for (char i = 0; i < 12; i++) {
            MeshSceneNode *turret = new MeshSceneNode();
            turret->meshVertices = MODEL_TURRETS_VERTS;
            turret->meshNormals = MODEL_TURRETS_NORMS;
            turret->meshUvs = MODEL_TURRETS_TEXCOORDS;
            turret->triangleCount = MODEL_TURRETS_TRIS;
            turret->texture = &pallete3Tex;
            turret->getTransform().rot.y = 0.5f * i;
            rootNode->addChild(turret);

            turrets[i] = turret;
        }

        MK_ANIMAL(sheep1, &RenderManager::animalTex1, -16.0f, -8.0f, M_PI * 1.1f);
        MK_ANIMAL(sheep2, &RenderManager::animalTex2, 12.0f, 2.0f, M_PI * 1.4f);
        MK_ANIMAL(sheep3, &RenderManager::animalTex3, 17.0f, 2.3f, M_PI * 1.8f);
        MK_ANIMAL(sheep4, &RenderManager::animalTex4, 7.0f, 6.2f, M_PI * 0.7f);
        MK_ANIMAL(sheep5, &RenderManager::animalTex5, -8.0f, -5.2f, M_PI);
        MK_ANIMAL(sheep6, &RenderManager::animalTex6, -14.0f, 5.2f, M_PI * 1.1f);
        MK_ANIMAL(sheep7, &RenderManager::animalTex7, -4.0f, 17.2f, M_PI * 1.4f);
        MK_ANIMAL(sheep8, &RenderManager::animalTex8, -6.4f, 10.2f, M_PI * 1.7f);

        u32 frameTime = 0;
        u32 frameTimeAnimation = 0; //Starts when you select to kill/save

        MeshSceneNode *gunfire = nullptr;

        while (1) {
            if (animalState == EnumAnimalState::KILL) {
                //Spawn kill lasers
                if (frameTimeAnimation >= 380 && frameTimeAnimation < 392) {
                    MeshSceneNode *turrets = new MeshSceneNode();
                    turrets->meshVertices = MODEL_TURRETSLASERKILL_VERTS;
                    turrets->meshNormals = MODEL_TURRETSLASERKILL_NORMS;
                    turrets->meshUvs = MODEL_TURRETSLASERKILL_TEXCOORDS;
                    turrets->triangleCount = MODEL_TURRETSLASERKILL_TRIS;
                    turrets->texture = &pallete3Tex;
                    turrets->getTransform().rot.y = 0.5f * (frameTimeAnimation - 380);
                    rootNode->addChild(turrets);

                    turretLasers[frameTimeAnimation - 380] = turrets;
                }
                if (frameTimeAnimation >= 0 && frameTimeAnimation < 120) {
                    roomDoor->getTransform().pos.y = GdqRender::lerp(0.002f, roomDoor->getTransform().pos.y, 55.0f);

                    sheep1->jumpDirection = {0.1f, 0.0f, 0.0f};
                    sheep2->jumpDirection = {0.1f, 0.0f, 0.0f};
                    sheep3->jumpDirection = {0.1f, 0.0f, 0.0f};
                    sheep4->jumpDirection = {0.1f, 0.0f, 0.0f};
                    sheep5->jumpDirection = {0.1f, 0.0f, 0.0f};
                    sheep6->jumpDirection = {0.1f, 0.0f, 0.0f};
                    sheep7->jumpDirection = {0.1f, 0.0f, 0.0f};
                    sheep8->jumpDirection = {0.1f, 0.0f, 0.0f};
                } else if (frameTimeAnimation >= 120) {
                    roomDoor->getTransform().pos.y = GdqRender::lerp(0.4f, roomDoor->getTransform().pos.y, 0.0f);
                    roomTrapdoor->getTransform().rot.z = GdqRender::lerp(0.1f, roomTrapdoor->getTransform().rot.z, M_PI * 0.49f);
                    sheep1->applyGravity = true;
                    sheep2->applyGravity = true;
                    sheep3->applyGravity = true;
                    sheep4->applyGravity = true;
                    sheep5->applyGravity = true;
                    sheep6->applyGravity = true;
                    sheep7->applyGravity = true;
                    sheep8->applyGravity = true;
                    sheep1->gravityFloor = -220.0f;
                    sheep2->gravityFloor = -220.0f;
                    sheep3->gravityFloor = -220.0f;
                    sheep4->gravityFloor = -220.0f;
                    sheep5->gravityFloor = -220.0f;
                    sheep6->gravityFloor = -220.0f;
                    sheep7->gravityFloor = -220.0f;
                    sheep8->gravityFloor = -220.0f;
                    sheep1->jumpDirection = {0.0f, 0.0f, 0.0f};
                    sheep2->jumpDirection = {0.0f, 0.0f, 0.0f};
                    sheep3->jumpDirection = {0.0f, 0.0f, 0.0f};
                    sheep4->jumpDirection = {0.0f, 0.0f, 0.0f};
                    sheep5->jumpDirection = {0.0f, 0.0f, 0.0f};
                    sheep6->jumpDirection = {0.0f, 0.0f, 0.0f};
                    sheep7->jumpDirection = {0.0f, 0.0f, 0.0f};
                    sheep8->jumpDirection = {0.0f, 0.0f, 0.0f};
                }

                if (frameTimeAnimation > 450 && frameTimeAnimation < 520 && frameTimeAnimation % 5 == 0) {
                    f32 rx = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
                    f32 ry = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);

                    rx *= 50.0f; rx -= 12.0f;
                    ry *= 50.0f; ry -= 12.0f;

                    SmokeSceneNode *turrets = new SmokeSceneNode();
                    turrets->meshVertices = MODEL_SMOKE_VERTS;
                    turrets->meshNormals = MODEL_SMOKE_NORMS;
                    turrets->meshUvs = MODEL_SMOKE_TEXCOORDS;
                    turrets->triangleCount = MODEL_SMOKE_TRIS;
                    turrets->texture = &pallete3Tex;
                    turrets->getTransform().pos.x = rx;
                    turrets->getTransform().pos.z = ry;
                    turrets->getTransform().pos.y = -215.0f;
                    turrets->getTransform().scl = {0.01f, 0.01f, 0.01f};
                    //turrets->getTransform().rot.y = 0.5f * i;
                    rootNode->addChild(turrets);
                }

                if (frameTimeAnimation == 450) {
                    MP3Player_Stop();
                    MP3Player_PlayBuffer(gunfireaudio_bin, gunfireaudio_bin_size, NULL);
                    gunfire = new MeshSceneNode();
                    gunfire->meshVertices = MODEL_GUNFIRE_VERTS;
                    gunfire->meshNormals = MODEL_GUNFIRE_NORMS;
                    gunfire->meshUvs = MODEL_GUNFIRE_TEXCOORDS;
                    gunfire->triangleCount = MODEL_GUNFIRE_TRIS;
                    gunfire->texture = &pallete3Tex;
                    rootNode->addChild(gunfire);
                } else if (frameTimeAnimation > 450 && frameTimeAnimation < 490) {
                    gunfire->getTransform().rot.y += 0.75f;
                }

                if (frameTimeAnimation == 490) {
                    //Hide sheep
                    sheep1->getTransform().scl = {0.0f, 0.0f, 0.0f};
                    sheep2->getTransform().scl = {0.0f, 0.0f, 0.0f};
                    sheep3->getTransform().scl = {0.0f, 0.0f, 0.0f};
                    sheep4->getTransform().scl = {0.0f, 0.0f, 0.0f};
                    sheep5->getTransform().scl = {0.0f, 0.0f, 0.0f};
                    sheep6->getTransform().scl = {0.0f, 0.0f, 0.0f};
                    sheep7->getTransform().scl = {0.0f, 0.0f, 0.0f};
                    sheep8->getTransform().scl = {0.0f, 0.0f, 0.0f};

                    //Hide lasers
                    for (int i = 0; i < 12; i++) {
                        turretLasers[i]->getTransform().scl = {0.0f, 0.0f, 0.0f};
                    }

                    //Hide gunfire
                    gunfire->getTransform().scl = {0.0f, 0.0f, 0.0f};
                } else if (frameTimeAnimation > 800 && frameTimeAnimation < 900) {
                    f32 near = GdqRender::lerp((frameTimeAnimation - 800.0f) / 100.0f, 300.0f, 0.0f);
                    f32 far = GdqRender::lerp((frameTimeAnimation - 800.0f) / 100.0f, 1500.0f, 0.1f);

                    f32 r = GdqRender::lerp((frameTimeAnimation - 800.0f) / 100.0f, 132.0f, 0.0f);
                    f32 g = GdqRender::lerp((frameTimeAnimation - 800.0f) / 100.0f, 179.0f, 0.0f);
                    f32 b = GdqRender::lerp((frameTimeAnimation - 800.0f) / 100.0f, 255.0f, 0.0f);

                    GX_SetFog(GX_FOG_LIN, near, far, 2.0f, 2000.0f, {r, g, b, 255});
                    RenderManager::backgroundColor = {r, g, b, 255};
                } else if (frameTimeAnimation == 900) {
                    break;
                }

                frameTimeAnimation++;
            } else if (animalState == EnumAnimalState::SAVE) {
                //Spawn save lasers
                if (frameTimeAnimation == 0) {
                    for (char i = 0; i < 12; i++) {
                        MeshSceneNode *turrets = new MeshSceneNode();
                        turrets->meshVertices = MODEL_TURRETSLASERSAVE_VERTS;
                        turrets->meshNormals = MODEL_TURRETSLASERSAVE_NORMS;
                        turrets->meshUvs = MODEL_TURRETSLASERSAVE_TEXCOORDS;
                        turrets->triangleCount = MODEL_TURRETSLASERSAVE_TRIS;
                        turrets->texture = &pallete3Tex;
                        turrets->getTransform().rot.y = 0.5f * i;
                        rootNode->addChild(turrets);

                        turretLasers[i] = turrets;
                    }
                }

                if (frameTimeAnimation >= 0 && frameTimeAnimation < 120) {
                    roomDoor->getTransform().pos.y = GdqRender::lerp(0.002f, roomDoor->getTransform().pos.y, 55.0f);

                    sheep1->jumpDirection = {0.1f, 0.0f, 0.0f};
                    sheep2->jumpDirection = {0.1f, 0.0f, 0.0f};
                    sheep3->jumpDirection = {0.1f, 0.0f, 0.0f};
                    sheep4->jumpDirection = {0.1f, 0.0f, 0.0f};
                    sheep5->jumpDirection = {0.1f, 0.0f, 0.0f};
                    sheep6->jumpDirection = {0.1f, 0.0f, 0.0f};
                    sheep7->jumpDirection = {0.1f, 0.0f, 0.0f};
                    sheep8->jumpDirection = {0.1f, 0.0f, 0.0f};
                } else if (frameTimeAnimation >= 120) {
                    if (frameTimeAnimation == 120) {
                        sheep1->applyGravity = true;
                        sheep2->applyGravity = true;
                        sheep3->applyGravity = true;
                        sheep4->applyGravity = true;
                        sheep5->applyGravity = true;
                        sheep6->applyGravity = true;
                        sheep7->applyGravity = true;
                        sheep8->applyGravity = true;
                        sheep1->gravityFloor = -220.0f;
                        sheep2->gravityFloor = -220.0f;
                        sheep3->gravityFloor = -220.0f;
                        sheep4->gravityFloor = -220.0f;
                        sheep5->gravityFloor = -220.0f;
                        sheep6->gravityFloor = -220.0f;
                        sheep7->gravityFloor = -220.0f;
                        sheep8->gravityFloor = -220.0f;
                        sheep1->jumpDirection = {0.0f, 0.0f, 0.0f};
                        sheep2->jumpDirection = {0.0f, 0.0f, 0.0f};
                        sheep3->jumpDirection = {0.0f, 0.0f, 0.0f};
                        sheep4->jumpDirection = {0.0f, 0.0f, 0.0f};
                        sheep5->jumpDirection = {0.0f, 0.0f, 0.0f};
                        sheep6->jumpDirection = {0.0f, 0.0f, 0.0f};
                        sheep7->jumpDirection = {0.0f, 0.0f, 0.0f};
                        sheep8->jumpDirection = {0.0f, 0.0f, 0.0f};
                        sheep1->shouldJump = false;
                        sheep2->shouldJump = false;
                        sheep3->shouldJump = false;
                        sheep4->shouldJump = false;
                        sheep5->shouldJump = false;
                        sheep6->shouldJump = false;
                        sheep7->shouldJump = false;
                        sheep8->shouldJump = false;
                        sheep1->jumpAccel = 0.0f;
                        sheep2->jumpAccel = 0.0f;
                        sheep3->jumpAccel = 0.0f;
                        sheep4->jumpAccel = 0.0f;
                        sheep5->jumpAccel = 0.0f;
                        sheep6->jumpAccel = 0.0f;
                        sheep7->jumpAccel = 0.0f;
                        sheep8->jumpAccel = 0.0f;
                    } else if (frameTimeAnimation == 140) {
                        sheep1->getTransform().scl = {12.0f, 12.0f, 12.0f};
                        sheep2->getTransform().scl = {12.0f, 12.0f, 12.0f};
                        sheep3->getTransform().scl = {12.0f, 12.0f, 12.0f};
                        sheep4->getTransform().scl = {12.0f, 12.0f, 12.0f};
                        sheep5->getTransform().scl = {12.0f, 12.0f, 12.0f};
                        sheep6->getTransform().scl = {12.0f, 12.0f, 12.0f};
                        sheep7->getTransform().scl = {12.0f, 12.0f, 12.0f};
                        sheep8->getTransform().scl = {12.0f, 12.0f, 12.0f};
                    }
                    roomDoor->getTransform().pos.y = GdqRender::lerp(0.4f, roomDoor->getTransform().pos.y, 0.0f);
                    roomTrapdoor->getTransform().rot.z = GdqRender::lerp(0.1f, roomTrapdoor->getTransform().rot.z, M_PI * 0.49f);
                }

                if (frameTimeAnimation >= 350 && frameTimeAnimation <= 375) {
                    int i = frameTimeAnimation - 350;
                    //Rotate around
                    f32 x = cos(i * 0.5f + 0.05f) * cos(0.0f) * 140.0f;
                    f32 z = sin(i * 0.5f + 0.05f) * cos(0.0f) * 140.0f;

                    SmokeSceneNode *turrets = new SmokeSceneNode();
                    turrets->meshVertices = MODEL_SMOKE_VERTS;
                    turrets->meshNormals = MODEL_SMOKE_NORMS;
                    turrets->meshUvs = MODEL_SMOKE_TEXCOORDS;
                    turrets->triangleCount = MODEL_SMOKE_TRIS;
                    turrets->texture = &pallete3Tex;
                    turrets->getTransform().pos.x = x;
                    turrets->getTransform().pos.z = z;
                    turrets->getTransform().pos.y = -215.0f;
                    turrets->getTransform().rot.y = 0.5f * i;
                    turrets->getTransform().scl = {0.01f, 0.01f, 0.01f};
                    //turrets->getTransform().rot.y = 0.5f * i;
                    rootNode->addChild(turrets);
                }

                if (frameTimeAnimation == 340) {
                    MP3Player_Stop();
                    MP3Player_PlayBuffer(gunfireaudio_bin, gunfireaudio_bin_size, NULL);
                    gunfire = new MeshSceneNode();
                    gunfire->meshVertices = MODEL_GUNFIRE_VERTS;
                    gunfire->meshNormals = MODEL_GUNFIRE_NORMS;
                    gunfire->meshUvs = MODEL_GUNFIRE_TEXCOORDS;
                    gunfire->triangleCount = MODEL_GUNFIRE_TRIS;
                    gunfire->getTransform().pos.y = 8.0f;
                    gunfire->texture = &pallete3Tex;
                    rootNode->addChild(gunfire);
                } else if (frameTimeAnimation > 350 && frameTimeAnimation < 395) {
                    gunfire->getTransform().rot.y += 0.75f;
                }

                if (frameTimeAnimation >= 370 && frameTimeAnimation < 382) {
                    int i = frameTimeAnimation - 370;
                    turrets[i]->getTransform().scl = {0.0f, 0.0f, 0.0f};
                    turretLasers[i]->getTransform().scl = {0.0f, 0.0f, 0.0f};

                    if (frameTimeAnimation == 380) {
                        gunfire->getTransform().scl = {0.0f, 0.0f, 0.0f};
                    }
                } else if (frameTimeAnimation == 470) {
                    sheep1->jumpDirection = {0.0f, 0.0f, 0.5f};
                    sheep2->jumpDirection = {0.0f, 0.0f, 0.5f};
                    sheep3->jumpDirection = {0.0f, 0.0f, 0.5f};
                    sheep4->jumpDirection = {0.0f, 0.0f, 0.5f};
                    sheep5->jumpDirection = {0.0f, 0.0f, 0.5f};
                    sheep6->jumpDirection = {0.0f, 0.0f, 0.5f};
                    sheep7->jumpDirection = {0.0f, 0.0f, 0.5f};
                    sheep8->jumpDirection = {0.0f, 0.0f, 0.5f};
                    sheep1->shouldJump = true;
                    sheep2->shouldJump = true;
                    sheep3->shouldJump = true;
                    sheep4->shouldJump = true;
                    sheep5->shouldJump = true;
                    sheep6->shouldJump = true;
                    sheep7->shouldJump = true;
                    sheep8->shouldJump = true;
                    sheep1->applyGravity = false;
                    sheep2->applyGravity = false;
                    sheep3->applyGravity = false;
                    sheep4->applyGravity = false;
                    sheep5->applyGravity = false;
                    sheep6->applyGravity = false;
                    sheep7->applyGravity = false;
                    sheep8->applyGravity = false;
                    sheep1->isJumping = 0;
                    sheep2->isJumping = 0;
                    sheep3->isJumping = 0;
                    sheep4->isJumping = 0;
                    sheep5->isJumping = 0;
                    sheep6->isJumping = 0;
                    sheep7->isJumping = 0;
                    sheep8->isJumping = 0;
                }

                //Fade
                if (frameTimeAnimation > 650 && frameTimeAnimation <= 750) {
                    //Rotating the camera is a faff, just fade to black instead :P
                    f32 near = GdqRender::lerp((frameTimeAnimation - 650.0f) / 100.0f, 300.0f, 0.0f);
                    f32 far = GdqRender::lerp((frameTimeAnimation - 650.0f) / 100.0f, 1500.0f, 0.1f);

                    GX_SetFog(GX_FOG_LIN, near, far, 2.0f, 2000.0f, {0, 0, 0, 255});
                    RenderManager::backgroundColor = {0, 0, 0, 255};

                    if (frameTimeAnimation == 750) {
                        RenderManager::camera = {-70.3664f, -0.97696f, -383.135f};
                        RenderManager::up = {0.0f, 1.0f, 0.0f};
                        RenderManager::look = {0.0f, 0.0f, -1000.855f};

                        sheep1->targetHeight = -20.0f;
                        sheep2->targetHeight = -20.0f;
                        sheep3->targetHeight = -20.0f;
                        sheep4->targetHeight = -20.0f;
                        sheep5->targetHeight = -20.0f;
                        sheep6->targetHeight = -20.0f;
                        sheep7->targetHeight = -20.0f;
                        sheep8->targetHeight = -20.0f;
                        sheep1->getTransform().pos.y = -20.0f;
                        sheep2->getTransform().pos.y = -20.0f;
                        sheep3->getTransform().pos.y = -20.0f;
                        sheep4->getTransform().pos.y = -20.0f;
                        sheep5->getTransform().pos.y = -20.0f;
                        sheep6->getTransform().pos.y = -20.0f;
                        sheep7->getTransform().pos.y = -20.0f;
                        sheep8->getTransform().pos.y = -20.0f;
                        sheep1->jumpDirection = {0.0f, 0.0f, -0.5f};
                        sheep2->jumpDirection = {0.0f, 0.0f, -0.5f};
                        sheep3->jumpDirection = {0.0f, 0.0f, -0.5f};
                        sheep4->jumpDirection = {0.0f, 0.0f, -0.5f};
                        sheep5->jumpDirection = {0.0f, 0.0f, -0.5f};
                        sheep6->jumpDirection = {0.0f, 0.0f, -0.5f};
                        sheep7->jumpDirection = {0.0f, 0.0f, -0.5f};
                        sheep8->jumpDirection = {0.0f, 0.0f, -0.5f};
                        sheep1->getTransform().pos.z -= 510.0f;
                        sheep2->getTransform().pos.z -= 510.0f;
                        sheep3->getTransform().pos.z -= 510.0f;
                        sheep4->getTransform().pos.z -= 510.0f;
                        sheep5->getTransform().pos.z -= 510.0f;
                        sheep6->getTransform().pos.z -= 510.0f;
                        sheep7->getTransform().pos.z -= 510.0f;
                        sheep8->getTransform().pos.z -= 510.0f;
                        sheep1->getTransform().scl = {20.0f, 20.0f, 20.0f};
                        sheep2->getTransform().scl = {20.0f, 20.0f, 20.0f};
                        sheep3->getTransform().scl = {20.0f, 20.0f, 20.0f};
                        sheep4->getTransform().scl = {20.0f, 20.0f, 20.0f};
                        sheep5->getTransform().scl = {20.0f, 20.0f, 20.0f};
                        sheep6->getTransform().scl = {20.0f, 20.0f, 20.0f};
                        sheep7->getTransform().scl = {20.0f, 20.0f, 20.0f};
                        sheep8->getTransform().scl = {20.0f, 20.0f, 20.0f};
                    }
                } else if (frameTimeAnimation > 750 && frameTimeAnimation <= 850) {
                    f32 near = GdqRender::lerp((frameTimeAnimation - 750.0f) / 100.0f, 0.0f, 300.0f);
                    f32 far = GdqRender::lerp((frameTimeAnimation - 750.0f) / 100.0f, 0.1f, 1500.0f);

                    f32 r = GdqRender::lerp((frameTimeAnimation - 750.0f) / 100.0f, 0.0f, 132.0f);
                    f32 g = GdqRender::lerp((frameTimeAnimation - 750.0f) / 100.0f, 0.0f, 179.0f);
                    f32 b = GdqRender::lerp((frameTimeAnimation - 750.0f) / 100.0f, 0.0f, 255.0f);

                    GX_SetFog(GX_FOG_LIN, near, far, 2.0f, 2000.0f, {(char) r, (char) g, (char) b, 255});
                    RenderManager::backgroundColor = {(char) r, (char) g, (char) b, 255};
                } else if (frameTimeAnimation > 1200 && frameTimeAnimation < 1300) {
                    f32 near = GdqRender::lerp((frameTimeAnimation - 1200.0f) / 100.0f, 300.0f, 0.0f);
                    f32 far = GdqRender::lerp((frameTimeAnimation - 1200.0f) / 100.0f, 1500.0f, 0.1f);

                    f32 r = GdqRender::lerp((frameTimeAnimation - 1200.0f) / 100.0f, 132.0f, 0.0f);
                    f32 g = GdqRender::lerp((frameTimeAnimation - 1200.0f) / 100.0f, 179.0f, 0.0f);
                    f32 b = GdqRender::lerp((frameTimeAnimation - 1200.0f) / 100.0f, 255.0f, 0.0f);

                    GX_SetFog(GX_FOG_LIN, near, far, 2.0f, 2000.0f, {r, g, b, 255});
                    RenderManager::backgroundColor = {r, g, b, 255};
                } else if (frameTimeAnimation == 1300) {
                    break;
                }

                frameTimeAnimation++;
            }

            //if (frameTimeAnimation == 240) {
                //MP3Player_Stop();
                //MP3Player_PlayBuffer(turretBuffer, turretSize, NULL);
            //}

            //Animate the camera
            if (frameTimeAnimation > 140 && frameTimeAnimation <= 240) {
                //Rotating the camera is a faff, just fade to black instead :P
                f32 near = GdqRender::lerp((frameTimeAnimation - 140.0f) / 100.0f, 300.0f, 0.0f);
                f32 far = GdqRender::lerp((frameTimeAnimation - 140.0f) / 100.0f, 1500.0f, 0.1f);

                GX_SetFog(GX_FOG_LIN, near, far, 2.0f, 2000.0f, {0, 0, 0, 255});
                RenderManager::backgroundColor = {132, 179, 255, 255};

                if (frameTimeAnimation == 240) {
                    RenderManager::camera = {-40.0, -200.268074f, -40.0f};
                    RenderManager::up = {0.0f, 1.0f, 0.0f};
                    RenderManager::look = {0.0f, -220.0f, 0.0f};
                }
            } else if (frameTimeAnimation > 240 && frameTimeAnimation <= 340) {
                f32 near = GdqRender::lerp((frameTimeAnimation - 240.0f) / 100.0f, 0.0f, 300.0f);
                f32 far = GdqRender::lerp((frameTimeAnimation - 240.0f) / 100.0f, 0.1f, 1500.0f);

                GX_SetFog(GX_FOG_LIN, near, far, 2.0f, 2000.0f, {0, 0, 0, 255});
            }

            RenderManager::draw(rootNode);

            PAD_Read(GdqRender::pads);
            //A to kill, B to save
            if (animalState == EnumAnimalState::PURGATORY) {
                if (GdqRender::pads[0].button & PAD_BUTTON_A) {
                    animalState = EnumAnimalState::KILL;
                    MP3Player_PlayBuffer(doorsBuffer, doorsSize, NULL);
                } else if (GdqRender::pads[0].button & PAD_BUTTON_B) {
                    animalState = EnumAnimalState::SAVE;
                    MP3Player_PlayBuffer(doorsBuffer, doorsSize, NULL);
                }
            }

            int sprite = (frameTime / 10) % 3;
            switch (sprite) {
                case 0:
                    portal->texture = &RenderManager::portalO1Tex; break;
                case 1:
                    portal->texture = &RenderManager::portalO2Tex; break;
                case 2:
                    portal->texture = &RenderManager::portalO3Tex; break;
            }

            frameTime++;
        }

        delete rootNode;
        //free(turretBuffer);
        free(doorsBuffer);

        usleep(500000);
        GX_SetFog(GX_FOG_LIN, 2000.0f, 2000.0f, 2.0f, 2000.0f, {0, 0, 0, 255});
        return 0;
    }

    guVector calcForwardsVector(guVector rot) {
        return {
            cos(rot.y) * sin(rot.x),
                sin(rot.y),
                cos(rot.y) * cos(rot.x)
        };
    }

    guVector cross(guVector a, guVector b) {
        return {
            a.y * b.z - a.z * b.y,
                a.x * b.z - a.z * b.x,
                a.x * b.y - a.y * b.x
        };
    }
}
