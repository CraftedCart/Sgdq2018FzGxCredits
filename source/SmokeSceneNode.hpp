#ifndef GDQ_RENDER_SMOKESCENENODE_HPP
#define GDQ_RENDER_SMOKESCENENODE_HPP

#include "MeshSceneNode.hpp"
#include <cstdlib>

class SmokeSceneNode : public MeshSceneNode {
    public:
        f32 scaleMultiplier = 0.2f;

    public:
        virtual void tick() override;
};

#endif

