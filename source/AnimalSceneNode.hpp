#ifndef GDQ_RENDER_ANIMALSCENENODE_HPP
#define GDQ_RENDER_ANIMALSCENENODE_HPP

#include "MeshSceneNode.hpp"
#include <cstdlib>

class AnimalSceneNode : public MeshSceneNode {
    public:
        bool shouldJump = true;
        u32 jumpTime = rand();
        f32 jumpAccel = 0.0f;
        char isJumping = false;
        f32 targetHeight;
        bool applyGravity = false;
        f32 gravityFloor;
        guVector jumpDirection = {0.0f, 0.0f, 0.0f};
        float gravity = 0.05f;

    public:
        virtual void tick() override;
};

#endif

