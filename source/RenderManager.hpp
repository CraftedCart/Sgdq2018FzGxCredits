#ifndef GDQ_RENDER_RENDERMANAGER_HPP
#define GDQ_RENDER_RENDERMANAGER_HPP

#include "MeshSceneNode.hpp"

namespace RenderManager {
    extern GXRModeObj *screenMode;
    extern void *frameBuffer;
    extern vu8 readyForCopy;
    extern Mtx viewMatrix;
    extern Mtx projectionMatrix;
    extern guVector camera;
    extern guVector up;
    extern guVector look;
    extern GXColor backgroundColor;
    extern void *fifoBuffer;
    extern guVector lightPos;
    extern GXColor litColors[3];

    extern GXTexObj pallete2Tex;
    extern GXTexObj animalTex1;
    extern GXTexObj animalTex2;
    extern GXTexObj animalTex3;
    extern GXTexObj animalTex4;
    extern GXTexObj animalTex5;
    extern GXTexObj animalTex6;
    extern GXTexObj animalTex7;
    extern GXTexObj animalTex8;
    extern GXTexObj portalGunTex;
    extern GXTexObj portalB1Tex;
    extern GXTexObj portalB2Tex;
    extern GXTexObj portalB3Tex;
    extern GXTexObj portalO1Tex;
    extern GXTexObj portalO2Tex;
    extern GXTexObj portalO3Tex;

    void init();

    void useGx();
    void useConsole();

    void draw(SceneNode *rootNode);
    void drawSceneGraph(SceneNode *rootNode, Mtx transformMatrix);
    void drawMeshNode(MeshSceneNode *node, Mtx transformMatrix);
    void setLight(Mtx view, guVector lightPos, GXColor litCol, GXColor ambCol, GXColor matCol);
    GXTexObj loadTplTextureFromMemory(const u8 tpl[], const u32 length, const s32 textureId);
    void copyBuffers(u32 count __attribute__ ((unused)));
}

#endif

