#include "AnimalSceneNode.hpp"

void AnimalSceneNode::tick() {
    jumpTime++;

    if (applyGravity) {
        jumpAccel -= gravity;

        if (getTransform().pos.y < gravityFloor) {
            getTransform().pos.y = gravityFloor;
            applyGravity = false;
            jumpAccel = 0.0f;
        }
    } else if (shouldJump) {
        if (jumpTime % (60 * 2) == 0) {
            jumpAccel = 0.8f;
            isJumping = 3;

            targetHeight = getTransform().pos.y;
        }

        if (isJumping == 3) {
            jumpAccel -= gravity;

            if (getTransform().pos.y < targetHeight) {
                isJumping = 2;
                jumpAccel = 0.4f;
                getTransform().pos.y = targetHeight;
            }
        } else if (isJumping == 2) {
            jumpAccel -= gravity;

            if (getTransform().pos.y < targetHeight) {
                isJumping = 1;
                jumpAccel = 0.2f;
                getTransform().pos.y = targetHeight;
            }
        } else if (isJumping == 1) {
            jumpAccel -= gravity;

            if (getTransform().pos.y < targetHeight) {
                isJumping = 0;
                jumpAccel = 0.0f;
                getTransform().pos.y = targetHeight;
            }
        }
    }

    getTransform().pos.y += jumpAccel;
    if (isJumping > 0) {
        getTransform().pos.x += jumpDirection.x;
        getTransform().pos.y += jumpDirection.y;
        getTransform().pos.z += jumpDirection.z;
    }
}

